﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObjects : MonoBehaviour {

    public enum rotationAxis {xAxis, yAxiis, zAxis};
    public rotationAxis axis = rotationAxis.xAxis;
    public float rotationSpeed = 15.0f;

	void Update () {
	    switch (axis) {
	            case rotationAxis.xAxis: 
                    transform.rotation *= Quaternion.Euler(rotationSpeed * Time.deltaTime, 0, 0);
	                break;
                case rotationAxis.yAxiis:
                    transform.rotation *= Quaternion.Euler(0, rotationSpeed * Time.deltaTime, 0);
                    break;
                case rotationAxis.zAxis:
                    transform.rotation *= Quaternion.Euler(0, 0, rotationSpeed * Time.deltaTime);
                    break;
                default: 
                    transform.rotation *= Quaternion.Euler(0, 0, 0);
                    break;
	    }
		
	}
}
