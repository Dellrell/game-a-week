﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockMouse : MonoBehaviour {

    public Texture2D cursorTex; 

    void Start() {
       Cursor.SetCursor(cursorTex, Vector2.zero, CursorMode.Auto);
    }

	void Update () {
	    Cursor.lockState = CursorLockMode.Locked;
	    Cursor.visible = false;
	    if (MenuManager.Instance.gameisPaused) {
	        Cursor.lockState = CursorLockMode.None;
	        Cursor.visible = true;
	    }
	}
}
