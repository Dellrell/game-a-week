﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public static MenuManager Instance;

    public bool gameisPaused;
    
	void Awake () {
	    if (Instance == null)
	        Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
            PauseTheGame();
	}

    public void PauseTheGame() {
        gameisPaused = !gameisPaused;
        Time.timeScale = gameisPaused ? 0 : 1;
    }
}
