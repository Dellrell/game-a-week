﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractWithEnvironment : MonoBehaviour {

    public GameObject player;
    public GameObject sun;

    public float hitDistance = 3;
    
    private int layerMask9 = 1 << 9;
    private int layerMask10 = 1 << 10;
    private int layerMaskInteractable;
    private bool pickupableHit;
    private bool interactableHit;
    private bool holdingObject;

    private GameObject hitObj;
    private Rigidbody hitRigid;
    private RaycastHit rayHit1;
    private RaycastHit rayHit2;

    private Vector3 lastGrabbedPosition;
    private Vector3 throwVector;
    private float throwForce;
    public float throwForceModFactor = 0.2f;
    private Vector3 throwVelocity;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	    //pickup raycast
	    pickupableHit = Physics.Raycast(transform.position, transform.forward, out rayHit1, hitDistance, layerMask9);
	    //environmet manipulator raycast
	    interactableHit = Physics.Raycast(transform.position, transform.forward, out rayHit2, hitDistance, layerMask10);
        
	    if (pickupableHit) {
	        if (Input.GetButtonDown("Fire1") && !holdingObject) {
	            hitObj = rayHit1.transform.gameObject;
	            hitRigid = hitObj.GetComponent<Rigidbody>();
	        }
	        if (Input.GetButton("Fire1")  && !holdingObject) {
	            holdingObject = true;
	        }
	    } else  {
	        holdingObject = false;
	    }

	    if (Input.GetButtonUp("Fire1") && holdingObject) {
	        holdingObject = false;
	        hitRigid.constraints = RigidbodyConstraints.None;
	        throwVector = hitObj.transform.position - lastGrabbedPosition;
	        throwForce = (throwVector.magnitude / Time.deltaTime) * throwForceModFactor;
	        throwVelocity = throwVector.normalized * throwForce;
	        hitRigid.velocity = throwVelocity;
	    }

	    if (holdingObject) {
	        hitObj.transform.SetParent(transform);
	        hitRigid.useGravity = false;
	        hitRigid.constraints = RigidbodyConstraints.FreezeAll;
	        lastGrabbedPosition = hitObj.transform.position;
	    }
	    else if (hitObj != null && hitRigid != null){
	        hitObj.transform.SetParent(null);
	        hitRigid.constraints = RigidbodyConstraints.None;
	        hitRigid.useGravity = true;
	    }
        
        if (interactableHit) {
            hitObj = rayHit2.transform.gameObject;
            if (Input.GetButton("Fire1")) {
                hitObj.transform.rotation *= Quaternion.Euler(0, 25*Time.deltaTime, 0);
                sun.transform.rotation *= Quaternion.Euler(25*Time.deltaTime, 0, 0);
            }
        }
        
	}
}
