﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PickupableObject : MonoBehaviour {
    
    private Vector3 startPos;

	// Use this for initialization
	void Awake () {
	    startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	    if (transform.position.y < -5)
	        transform.position = startPos;
	}
}
