﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySwitch : MonoBehaviour {
    
    public PlayerMovement playerMovement;
    public CharacterController PlayerController;
    public MouseLook MouseLookX;
    public MouseLook MouseLookY;

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            StartCoroutine(RotatePlayer(5));
            playerMovement.gravity = new Vector3(0, 0, 10);
            PlayerController.radius = 0.5f;
            PlayerController.height = 0;
            PlayerController.center = new Vector3(0, -0.5f, 0);
            //MouseLookX.initialRotation = Quaternion.Euler(90, 0, 0);
            //MouseLookY.initialRotation = Quaternion.Euler(90, 90, 0);
            MouseLookY.minimumY = 0;
            MouseLookY.maximumY = 160;
            playerMovement.Switched = true;

        }
    }

    private IEnumerator RotatePlayer(float duration) {
        float timer = 0;
        while (timer < duration) {
            timer += Time.deltaTime;
            MouseLookX.initialRotation = Quaternion.Slerp(MouseLookX.initialRotation, Quaternion.Euler(90, 0, 0), timer / duration);
            MouseLookY.initialRotation = Quaternion.Slerp(MouseLookY.initialRotation, Quaternion.Euler(90, 90, 0), timer / duration);
            yield return null;
        }
    }
}
