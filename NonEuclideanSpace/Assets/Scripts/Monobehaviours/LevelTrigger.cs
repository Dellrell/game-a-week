﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTrigger : MonoBehaviour {

    public GameObject levelToSwitchOff;
    public GameObject levelToSwitchOn;

    public GameObject playerLookChecker;
    private LookCheck check;

    private bool levelReady;

    private void Start() {
        if (playerLookChecker != null)
            check = playerLookChecker.GetComponent<LookCheck>();

    }

    private void Update() {
        
        if (playerLookChecker == null) {
            levelReady = true;

        }  else if (playerLookChecker != null) {
            if(check.playerLooking)
                levelReady = true;

        } else {
            levelReady = false;
        }

    }

    private IEnumerator SwitchLevels() {
        yield return new WaitForEndOfFrame();
        levelToSwitchOn.SetActive(true);
        levelToSwitchOff.SetActive(false);
    }

    void OnTriggerStay(Collider other) {
        if (other.tag == "Player" && levelReady)
            StartCoroutine(SwitchLevels());
    }
}
