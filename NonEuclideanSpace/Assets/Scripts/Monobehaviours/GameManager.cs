﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject startScreen;
    public GameObject pauseScreen;
    public bool gamePaused;

    //private EndTimer endTimer;
    private PlayerMovement playerMovement;
    private MouseLook mouseLookPlayer;
    private MouseLook mouseLookCamera;

    private void Start() {
        //endTimer = GetComponent<EndTimer>();
        playerMovement = transform.parent.GetComponent<PlayerMovement>();
        mouseLookPlayer = transform.parent.GetComponent<MouseLook>();
        mouseLookCamera = GetComponent<MouseLook>();

        if (startScreen.activeInHierarchy) {
            //endTimer.enabled = false;
            gamePaused = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else {
            gamePaused = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape))
            PauseGame();

        playerMovement.enabled = !gamePaused;
        mouseLookPlayer.enabled = !gamePaused;
        mouseLookCamera.enabled = !gamePaused;
    }

    public void PauseGame() {
        if (!gamePaused) {
            pauseScreen.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            gamePaused = true;
        }
        else {
            pauseScreen.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            gamePaused = false;
        }

    }

    public void StartGame() {
        startScreen.SetActive(false);
        //endTimer.enabled = true;
        gamePaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void RestartGame() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
