﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitchTrigger : MonoBehaviour {

    public GameObject MirrorToSwitchOn;
    public GameObject MirrorToSwitchOff;

    public GameObject CameraToSwitchOff;
    public GameObject CameraToSwitchOn;

    public GameObject AdditionalObjectToSwitchOn;

    public LookCheck Check;

    private void OnTriggerStay(Collider other) {
        if (other.tag == "Player") {
            if (Check == null) {
                StartCoroutine(SwitchCameras());
            } else if (Check.playerLooking) {
                StartCoroutine(SwitchCameras());
            }

        }
    }

    private IEnumerator SwitchCameras() {
        yield return new WaitForEndOfFrame();
        CameraToSwitchOff.SetActive(false);
        CameraToSwitchOn.SetActive(true);

        if(MirrorToSwitchOff != null)
            MirrorToSwitchOff.SetActive(false);

        if(MirrorToSwitchOn != null)
            MirrorToSwitchOn.SetActive(true);

        if(AdditionalObjectToSwitchOn != null)
            AdditionalObjectToSwitchOn.SetActive(true);
    }
}
