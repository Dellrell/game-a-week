﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour {

    public enum RotationAxis {xAxis, yAxis, zAxis};

    public RotationAxis axis = RotationAxis.yAxis;

    public bool open;
    public bool positive;
    public Quaternion openRotation;
    
    private OpenDoors openDoors;
    private float timer;
    
    private void Start() {
        openDoors = FindObjectOfType<OpenDoors>();

        int flag = positive ? 1 : -1;

        switch (axis) {
            case RotationAxis.xAxis:
                openRotation = Quaternion.Euler(120 * flag, 0, 0);
                break;
            case RotationAxis.yAxis:
                openRotation = Quaternion.Euler(0, 120 * flag, 0);
                break;
            case RotationAxis.zAxis:
                openRotation = Quaternion.Euler(0, 0, 120 * flag);
                break;
            default:
                openRotation = Quaternion.Euler(0, 120 * flag, 0);
                break;
        }
    }

    private void Update() {

        timer = open ? timer += Time.deltaTime : 0;

        if (open) {
            if (timer >= 3) {
                StartCoroutine(openDoors.DoorSwing(Quaternion.identity, transform, this, 2.0f));
                open = !open;
            }
        }
    }
}
