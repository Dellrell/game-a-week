﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchRooms : MonoBehaviour {
    
    public GameObject Level5Walls;
    public GameObject Level5Furniture;
    public GameObject Level6Walls;
    public GameObject Level6Furniture;

    public GameObject Camera5;
    public GameObject Camera6;

    public GameObject Instructions;

    private OpenDoors openDoors;

    private Vector3 startPosition;
    private Vector3 switchPosition = new Vector3(0.008f, -0.11f, 0.353f);
    private bool switching;

	void Start () {
	    openDoors = FindObjectOfType<OpenDoors>();
        startPosition = new Vector3(transform.localPosition.x, -0.11f, transform.localPosition.z);
	}
	
	void Update () {
		if (!openDoors.MirrorActive) return;

	    if (Input.GetButtonUp("Fire2") && !switching) {
	        StartCoroutine(SwitchTheRooms(1));
            Instructions.SetActive(false);
	    }
	}

    private IEnumerator SwitchTheRooms(float duration) {
        switching = true;

        float timer = 0;
        while (timer < duration) {
            timer += Time.deltaTime;
            transform.localPosition = Vector3.Lerp(transform.localPosition, switchPosition, timer / duration);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(0, 180, 0), timer / duration );
            yield return null;
        }
        Level5Walls.SetActive(!Level5Walls.activeInHierarchy);
        Level5Furniture.SetActive(!Level5Furniture.activeInHierarchy);
        Level6Walls.SetActive(!Level6Walls.activeInHierarchy);
        Level6Furniture.SetActive(!Level6Furniture.activeInHierarchy);
        Camera5.SetActive(!Camera5.activeInHierarchy);
        Camera6.SetActive(!Camera6.activeInHierarchy);
        timer = 0;
        while (timer < duration) {
            timer += Time.deltaTime;
            transform.localPosition = Vector3.Lerp(transform.localPosition, startPosition, timer / duration);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, timer / duration );
            yield return null;
        }

        switching = false;
    }
}
