﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class SceneTrigger : MonoBehaviour {

    public GameObject TiltedRoom;
    public GameObject RealRoom;
    public GameObject Player;

    public RenderTextureSetup MirrorSetup;
    public HandTextureSetup HandMirrorSetup;

    private PlayerMovement playerMovement;
    private CharacterController characterController;
    private MouseLook mouseLookX;
    private MouseLook mouseLookY;
    
    private static Vector3 playerPos;
    private static Quaternion playerRot;

    private void Awake() {
        playerMovement = Player.GetComponent<PlayerMovement>();
        characterController = Player.GetComponent<CharacterController>();
        mouseLookX = Player.GetComponent<MouseLook>();
        mouseLookY = Player.transform.GetChild(0).GetComponent<MouseLook>();
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag != "Player") return;

        Player.transform.parent = TiltedRoom.transform;
        playerPos = Player.transform.localPosition;
        playerRot = Player.transform.localRotation;
        
        Player.transform.parent = RealRoom.transform;
        playerMovement.gravity = new Vector3(0, 10, 0);
        Player.transform.localPosition = playerPos;
        Player.transform.localRotation = playerRot;
        characterController.radius = 0.5f;
        characterController.height = 2;
        characterController.center = Vector3.zero;
        mouseLookY.minimumY = 0;
        mouseLookY.maximumY = 160;

        MirrorSetup.enabled = false;
        HandMirrorSetup.enabled = true;

        //SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
	
}
