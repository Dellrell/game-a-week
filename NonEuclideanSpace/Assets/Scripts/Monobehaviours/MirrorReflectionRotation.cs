﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorReflectionRotation : MonoBehaviour {

    public Transform PlayerCamera;
    public Quaternion CameraRotation;
    
	
	private void Update () {
	    CameraRotation = GetRotation();
	}

    private Quaternion GetRotation() {
        Vector3 dir = (PlayerCamera.position - transform.position).normalized;
        Quaternion rotation = Quaternion.LookRotation(dir);

        rotation.eulerAngles = transform.eulerAngles - rotation.eulerAngles;
        return rotation;
    }
}
