﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandTextureSetup : MonoBehaviour {

    public Material RenderTextureMaterial;

    public Camera Room5Camera;
    public Camera Room6Camera;

    private RenderTexture cam5RenderTexture;
    private RenderTexture cam6RenderTexture;

	void Start () {
	    if (Room5Camera.targetTexture != null) 
            Room5Camera.targetTexture.Release();
	    if (Room6Camera.targetTexture != null)
            Room6Camera.targetTexture.Release();

	    cam5RenderTexture = new RenderTexture(Screen.width, Screen.height, 24);
	    cam6RenderTexture = new RenderTexture(Screen.width, Screen.height, 24);

	    Room5Camera.targetTexture = cam5RenderTexture;
	    Room6Camera.targetTexture = cam6RenderTexture;

	    RenderTextureMaterial.mainTexture = Room6Camera.targetTexture;

	}
	

	void Update () {
	    if (Room5Camera.isActiveAndEnabled) {
	        SwitchToCameraFive();
	    } else if (Room6Camera.isActiveAndEnabled) {
	        SwitchToCameraSix();
	    }
		
	}

    private void SwitchToCameraSix() {
        RenderTextureMaterial.mainTexture = Room6Camera.targetTexture;
    }

    private void SwitchToCameraFive() {
        RenderTextureMaterial.mainTexture = Room5Camera.targetTexture;
    }
}
