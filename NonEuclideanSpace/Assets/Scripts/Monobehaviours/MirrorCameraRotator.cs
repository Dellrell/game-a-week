﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorCameraRotator : MonoBehaviour {

    public MirrorReflectionRotation ReferenceRotator;
    private Quaternion rotation;

	void Update () {
	    rotation = ReferenceRotator.CameraRotation;

	    transform.localRotation = rotation;
	}
}
