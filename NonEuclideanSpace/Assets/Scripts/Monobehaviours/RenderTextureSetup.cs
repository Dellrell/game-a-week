﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderTextureSetup : MonoBehaviour {
    
    public Material RenderTextureMaterial;

    public Camera Room1Camera;
    public Camera Room2Camera;
    public Camera Room3Camera;
    public Camera Room4Camera;

    private RenderTexture cam1RenderTexture;
    private RenderTexture cam2RenderTexture;
    private RenderTexture cam3RenderTexture;
    private RenderTexture cam4RenderTexture;

	void Start () {

        if(Room1Camera.targetTexture != null)
            Room1Camera.targetTexture.Release();
	    if(Room2Camera.targetTexture != null)
	        Room2Camera.targetTexture.Release();
	    if(Room3Camera.targetTexture != null)
	        Room3Camera.targetTexture.Release();
	    if(Room4Camera.targetTexture != null)
	        Room4Camera.targetTexture.Release();
	   
        cam1RenderTexture = new RenderTexture(Screen.width, Screen.height, 24);
	    cam2RenderTexture = new RenderTexture(Screen.width, Screen.height, 24);
	    cam3RenderTexture = new RenderTexture(Screen.width, Screen.height, 24);
	    cam4RenderTexture = new RenderTexture(Screen.width, Screen.height, 24);

	    Room1Camera.targetTexture = cam1RenderTexture;
	    Room2Camera.targetTexture = cam2RenderTexture;
	    Room3Camera.targetTexture = cam3RenderTexture;
	    Room4Camera.targetTexture = cam4RenderTexture;

	    RenderTextureMaterial.mainTexture = Room2Camera.targetTexture;
	}

    private void Update() {
        if (Room1Camera.isActiveAndEnabled) {
            SwitchToCameraOne();
        } else if (Room2Camera.isActiveAndEnabled) {
            SwitchToCameraTwo();
        }else if (Room3Camera.isActiveAndEnabled) {
            SwitchToCameraThree();
        } else if (Room4Camera.isActiveAndEnabled) {
            SwitchToCameraFour();
        }
    }

    private void SwitchToCameraOne() {
        RenderTextureMaterial.mainTexture = Room1Camera.targetTexture;
    }

    private void SwitchToCameraTwo() {
        RenderTextureMaterial.mainTexture = Room2Camera.targetTexture;
    }

    private void SwitchToCameraThree() {
        RenderTextureMaterial.mainTexture = Room3Camera.targetTexture;
    }

    private void SwitchToCameraFour() {
        RenderTextureMaterial.mainTexture = Room4Camera.targetTexture;
    }
	
}
