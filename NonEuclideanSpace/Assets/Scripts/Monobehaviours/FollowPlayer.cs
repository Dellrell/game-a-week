﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform PlayerCamera;
    
	void Update () {
	    transform.position = PlayerCamera.position;
	    transform.rotation = PlayerCamera.rotation;
	}
}
