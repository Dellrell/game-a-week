﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenDoors : MonoBehaviour {

    public Transform HandMirror;
    public bool MirrorActive;
    public GameObject Instructions;

    public Image reticle;
    private readonly Color noHitCol = new Color(0, 0, 0, 0.5f);
    private readonly Color hitCol = new Color(1, 1, 1, 0.5f);

    private RaycastHit hit;
    private RaycastHit cHit;
    private RaycastHit mHit;
    private const int doorMask = 1 << 9;
    private const int checkMask = 1 << 10;
    private const int mirrorMask = 1 << 13;
    private bool doorHit;
    private bool checkHit;
    private bool mirrorHit;
    private LookCheck check;

    private Transform doorTrans;
    private Quaternion closedRotation;
    private Quaternion openRotation;
    private DoorManager doorManager;
    private bool pauseInteractions;

	void Start () {
		closedRotation = Quaternion.Euler(0, 0, 0);
	}
	
	void Update () {
	    doorHit = Physics.Raycast(transform.position, transform.forward, out hit, 3, doorMask);
	    checkHit = Physics.Raycast(transform.position, transform.forward, out cHit, 8, checkMask);
	    mirrorHit = Physics.Raycast(transform.position, transform.forward, out mHit, 3, mirrorMask);
        //Debug.DrawRay(transform.position, transform.forward * 8, Color.yellow);

	    if (checkHit) {
	        check = cHit.transform.GetComponent<LookCheck>();
	        check.playerLooking = true;
	    } else if (check != null) {
	        check.playerLooking = false;
	        check = null;
	    }

	    if (doorHit && Input.GetButtonUp("Fire1")) {
	        doorTrans = hit.transform;
	        doorManager = doorTrans.GetComponent<DoorManager>();
	        openRotation = doorManager.openRotation;
	        Quaternion target = doorManager.open ? closedRotation : openRotation;
	        StartCoroutine(DoorSwing(target, doorTrans, doorManager, 1.0f));
	        doorManager.open = !doorManager.open;
	    }

	    if (mirrorHit && Input.GetButtonUp("Fire1")) {
            mHit.transform.gameObject.SetActive(false);
	        StartCoroutine(RaiseMirror(2));
	    }

	    reticle.color = doorHit || mirrorHit ? hitCol : noHitCol;

	}

    public IEnumerator DoorSwing(Quaternion targetRotation, Transform door, DoorManager manager, float duration) {
        float timer = 0;

        while (timer <= duration) {
            timer += Time.deltaTime;

            door.localRotation = Quaternion.Lerp(door.localRotation, targetRotation, timer / duration);

            yield return null;
        }
    }

    private IEnumerator RaiseMirror(float duration) {
        float timer = 0;

        while (timer <= duration) {
            timer += Time.deltaTime;

            HandMirror.localPosition = Vector3.Lerp(HandMirror.localPosition,
                new Vector3(HandMirror.localPosition.x, -0.11f, HandMirror.localPosition.z), timer / duration);

            yield return null;
        }

        MirrorActive = true;
        Instructions.SetActive(true);
    }
}
