﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour {

    public float speed = 5.0f;
    public float runSpeed = 10.0f;

    public Vector3 gravity = new Vector3(0, 10, 0);
    public bool Switched;
    
    //public float jumpSpeed = 6.0f;
    private float horizontalInput;
    private float verticalInput;
    private int jumpTimer;
    private int jumpReset = 1;
    private CharacterController controller;
    private Vector3 movement = Vector3.zero;
    private float currentSpeed;

    private int groundMask = 1 << 12;


	// Use this for initialization
	void Start () {
	    controller = GetComponent<CharacterController>();
	    currentSpeed = speed;
	}
	
	void FixedUpdate () {

	    bool grounded = Physics.Raycast(transform.position, -transform.up, 2.1f, groundMask);
        //Debug.DrawRay(transform.position, -transform.up * 2.1f, Color.magenta);

        //get input
	    horizontalInput = Input.GetAxis("Horizontal");
	    verticalInput = Input.GetAxis("Vertical");

	    float targetSpeed = Input.GetKey(KeyCode.LeftShift) ? runSpeed : speed;
	    currentSpeed = Mathf.Lerp(currentSpeed, targetSpeed, 0.1f);

	    if (grounded) {
            //apply input
	        movement = new Vector3(horizontalInput, 0, verticalInput);
            movement = transform.TransformDirection(movement) * currentSpeed;

	        if (Switched) {
	            movement = new Vector3(Input.GetAxis("Vertical"), 0, -Input.GetAxis("Horizontal"));
	            movement = transform.TransformDirection(movement) * currentSpeed;
	        }

            if (!Input.GetButton("Jump")) {
	            jumpTimer++;
	        } else if (jumpTimer >= jumpReset) {
	            movement += gravity * 0.5f;
	            jumpTimer = 0;
	        }

	    }

	    //apply gravity
	    movement -= gravity * Time.deltaTime;
	    //move controller
	    controller.Move(movement * Time.deltaTime);
    }
}
