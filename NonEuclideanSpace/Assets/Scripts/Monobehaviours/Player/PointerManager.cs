﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerManager : MonoBehaviour {
    
	void Start () {
	    Cursor.lockState = CursorLockMode.Locked;
	    Cursor.visible = false;
	}
	
	void Update () {
	    if (Input.GetKey(KeyCode.Escape)) {
	        Cursor.lockState = CursorLockMode.None;
	        Cursor.visible = true;
	    }
	    else {
	        Cursor.lockState = CursorLockMode.Locked;
	        Cursor.visible = false;
	    }
	}
}
