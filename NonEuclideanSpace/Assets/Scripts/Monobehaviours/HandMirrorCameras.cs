﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandMirrorCameras : MonoBehaviour {

    public Transform ReferenceTransform;
	
	void Update () {
	    transform.localPosition = ReferenceTransform.localPosition;
	    transform.localRotation = ReferenceTransform.localRotation;
	}
}
