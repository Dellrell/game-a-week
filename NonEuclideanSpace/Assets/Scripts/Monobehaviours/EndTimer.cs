﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTimer : MonoBehaviour {

    public AnimationCurve FadeCurve;
    
    private PlayerMovement playerMovement;

    public Image FadeOutPanel;

    private float timer;
    private const int RestartTime = 20;

    private GameManager gameManager;

    
	void Start () {
	    playerMovement = FindObjectOfType<PlayerMovement>();
	    gameManager = FindObjectOfType<GameManager>();
	}
	
	void Update () {
	  if(Input.anyKey || !Mathf.Approximately(Input.GetAxis("Mouse X"), 0) || !Mathf.Approximately(Input.GetAxis("Mouse Y"), 0)) {
	      timer = 0;
	  }
	    else {
	        timer += Time.deltaTime;
	        if (timer >= RestartTime) {
	            playerMovement.enabled = false;
	            StartCoroutine(FadeOutAndRestart(10));
	        }
	    }
	}

    private IEnumerator FadeOutAndRestart(float duration) {
        float fadeTimer = 0;
        while (fadeTimer <= duration) {
            fadeTimer += Time.deltaTime;
            FadeOutPanel.color = new Color(1, 1, 1, FadeCurve.Evaluate(fadeTimer)); 
            yield return null;
        }
        gameManager.RestartGame();
    }

}
