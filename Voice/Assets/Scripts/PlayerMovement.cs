﻿using UnityEngine;


[RequireComponent(typeof(CharacterController))]

public class PlayerMovement : MonoBehaviour {

    public float Speed = 5.0f;
    public float RunSpeed = 8.0f;
    public float Gravity = 10.0f;
    private float horizontalInput;
    private float verticalInput;
    private CharacterController controller;
    private Vector3 movement = Vector3.zero;

    private bool crouched; 


	// Use this for initialization
	void Start () {
	    controller = GetComponent<CharacterController>();
	}
	
	void FixedUpdate () {
        //get input
	    horizontalInput = Input.GetAxis("Horizontal");
	    verticalInput = Input.GetAxis("Vertical");

	    float theSpeed = Input.GetKey(KeyCode.LeftShift) && !crouched ? RunSpeed : Speed;
	    crouched = Input.GetKey(KeyCode.Space);

	    if (controller.isGrounded) {
            //apply input
	        movement = new Vector3(horizontalInput, 0, verticalInput);
            movement = transform.TransformDirection(movement) * theSpeed;

	    }

	    if (crouched) {
	        controller.height = Mathf.Lerp(controller.height, -1f, Time.deltaTime * 5);
	        controller.center = Vector3.Lerp(controller.center, Vector3.zero, Time.deltaTime * 5);
	    }
	    else {
	        controller.center = Vector3.Lerp(controller.center, Vector3.zero, Time.deltaTime * 5);
	        controller.height = Mathf.Lerp(controller.height, 2, Time.deltaTime * 5);
	    }

	    //apply gravity
	    movement.y -= Gravity * Time.deltaTime;
	    //move controller
	    controller.Move(movement * Time.deltaTime);

    }
}
