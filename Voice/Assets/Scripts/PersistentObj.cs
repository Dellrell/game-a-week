﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentObj : MonoBehaviour {

    public static PersistentObj Instance;

    void Awake() {
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
}
