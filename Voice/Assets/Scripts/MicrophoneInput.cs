﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class MicrophoneInput : MonoBehaviour {
    
    //volume of the player
    public static float MicVolume;

    //public float micSensitivity = 10f;

    //ui stuff
    public Dropdown micDropdown;
    private readonly List<string> options = new List<string>();

    //for inputing sound from mic and outputing it to the game 
    private string microphone;
    private AudioClip input;
    private AudioSource output;

    //audio visualisation stuff//
    public Slider VolumeDisplay;
    //for calculating volume
    private float maxSample = 0.7f;
    private float normalisedVolume;


	void Start () {
	    output = GetComponent<AudioSource>();

	    foreach (string mic in Microphone.devices) {
	        if (microphone == null) //add first mic by default
	            microphone = mic;

            options.Add(mic);
	    }

        micDropdown.AddOptions(options);
        micDropdown.onValueChanged.AddListener(delegate {ChangeMicrophone(micDropdown);});
        
        UpdateMicrophone();
	}

    void UpdateMicrophone() {
        output.Stop();

        input = Microphone.Start(microphone, true, 10, AudioSettings.outputSampleRate);
        output.clip = input;
        output.loop = true;

        if (Microphone.IsRecording(microphone)) {
            while (!(Microphone.GetPosition(microphone) > 0)) {} //wait for the device to actually start recording 
            //output the recorded sound 
            output.Play();
            //Debug.Log("using " + microphone);
        }
        else {
            Debug.Log(microphone + "doesn't seem to be working");
        }

    }

    void ChangeMicrophone(Dropdown dropdown) {
        microphone = options[dropdown.value];
        UpdateMicrophone();
    }

    private float GetVolume() {
        int sampleSize = 128;
        float[] data = new float[sampleSize];
        int micPosition = Microphone.GetPosition(microphone) - (sampleSize + 1);
        if (micPosition <= 0) return 0;

        output.GetOutputData(data, 0);
        float sampleVolume = 0;

        foreach (float sample in data) {
            sampleVolume += Mathf.Abs(sample);
            if (sample > maxSample) {
                maxSample = sample;
                //print(sample);
            }
        }
        sampleVolume /= sampleSize;

        float bufferedVolume = 0;
        float bufferDecrease = 0;

        if (bufferedVolume < sampleVolume) {
            bufferedVolume = sampleVolume;
            bufferDecrease = 0.005f;
        }
        if (bufferedVolume > sampleVolume) {
            bufferDecrease *= 1.2f;
            bufferedVolume -= bufferDecrease;
        }

        normalisedVolume = bufferedVolume / maxSample;

        return normalisedVolume;
    }

	
	void Update () {
	    if (Time.timeSinceLevelLoad < 1.5f) return; //wait half a second for the stupid microphone to pop

	    MicVolume = GetVolume();
        //Debug.Log(micVolume);
        if(VolumeDisplay != null)
            VolumeDisplay.value = MicVolume;

	}
}
