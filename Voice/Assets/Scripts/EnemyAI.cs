﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyAI : AbstractAI {

    private bool inPursuit;
    private float detectionTimer;
    private float detectionLimit;
    private Light detectionLight;
    private float walkSpeed = 3.5f;
    private float runSpeed = 5.5f;
    private bool playerSpotted;
    private bool playerHit;
    private int layerMask = 1 << 9;

    private void Start () {
	    agent = GetComponent<NavMeshAgent>();
	    detectionLimit = 5;
	    detectionTimer = detectionLimit;
        detectionLight = GetComponentInChildren<Light>();
       
    }

    // Update is called once per frame
    private void Update () {
        playerSpotted = Physics.Raycast(transform.position, transform.forward, Mathf.Infinity, layerMask);
        playerHit = Physics.Raycast(transform.position, transform.forward, 5, layerMask);
        Debug.DrawRay(transform.position, transform.forward, Color.yellow);

        if(!inPursuit) {
            Patrol();
            detectionLight.enabled = false;
        }

	    playerDetected = CalculateDetectionModifyer() > 0.8f;

	    if (playerDetected || playerSpotted) {detectionTimer = 0; /*print(gameObject.name);*/}
	       
	    if (detectionTimer < detectionLimit) {
	        detectionTimer += Time.deltaTime;
	        agent.SetDestination(Player.position);
	        detectionLight.enabled = true;
	        if (playerDetected || playerSpotted) {detectionTimer = 0;}
	        //Debug.Log(detectionTimer);
	    }

	    inPursuit = detectionTimer < detectionLimit;
        agent.speed = inPursuit ? runSpeed : walkSpeed;

        if(playerHit) {GameManager.Instance.EndGame();}
        
    }
}
