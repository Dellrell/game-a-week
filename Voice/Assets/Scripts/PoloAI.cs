﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AudioSource))]
public class PoloAI : AbstractAI {

    private AudioSource audioSource;
    private readonly WaitForSeconds soundDelay = new WaitForSeconds(1);
    private bool answering;
 

	// Use this for initialization
	void Start () {
	    agent = GetComponent<NavMeshAgent>();
	    audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        Patrol();
	    if ((patrolIndex + 1) % 2 != 0) {
	        agent.enabled = false;
	        transform.position = PatrolPoints[patrolIndex].position;
	        agent.enabled = true;
	    }


	    playerDetected = CalculateDetectionModifyer() > 0.5f;
	    if (playerDetected && !audioSource.isPlaying && !answering) {
	        StartCoroutine(CallBack());
	    }
	}

    private IEnumerator CallBack() {
        answering = true;
        yield return soundDelay;
        audioSource.Play();
        yield return null;
        answering = false;
    }
}
