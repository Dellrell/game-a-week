﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LockMouse : MonoBehaviour {

    private PauseGame pauseGame;

    void Start() {
        pauseGame = GetComponent<PauseGame>();
    }

	void Update () {
	    Cursor.lockState = CursorLockMode.Locked;
	    Cursor.visible = false;

	    if (pauseGame.GamePaused) {
	        Cursor.lockState = CursorLockMode.None;
	        Cursor.visible = true;
	    }
	}
}
