﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceStart : MonoBehaviour {

    public ButtonManager buttonManager;

	void Update () {
	    if (MicrophoneInput.MicVolume > 0.65f) {
            buttonManager.StartGame();
	    }
	}
}
