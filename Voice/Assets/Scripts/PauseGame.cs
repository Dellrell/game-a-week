﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

    public GameObject PauseMenu;
    public bool GamePaused;

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape))
            PauseTheGame();

        if (GamePaused) {
            Time.timeScale = 0;
            PauseMenu.SetActive(true);
        }
        else {
            PauseMenu.SetActive(false);
            Time.timeScale = 1;
        }

    }

    public void PauseTheGame() {
        GamePaused = !GamePaused;
    }
}
