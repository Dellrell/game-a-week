﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

    public void StartGame() {
        MicrophoneInput.MicVolume = 0;
        SceneManager.LoadScene(1, LoadSceneMode.Single);
        
    }

    public void ResetGame() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
