﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof( NavMeshAgent))]
public abstract class AbstractAI : MonoBehaviour {

    public Transform[] PatrolPoints;
    public Transform Player;

    protected NavMeshAgent agent;

    protected bool playerDetected;

    protected int patrolIndex;
    private readonly WaitForSeconds patrolDelay = new WaitForSeconds(1);
    protected bool IndexUpdted;

    //protected float distance;
    protected float maxDistance;
    protected float detectionModifyer;


    protected float CalculateDetectionModifyer() {
        float rawDist = Vector3.Distance(transform.position, Player.position);
        if (rawDist > maxDistance)
            maxDistance = rawDist;
        float normalisedDistance = rawDist / maxDistance;
        float reverseDistance = 2 - normalisedDistance;
        
        detectionModifyer = reverseDistance * MicrophoneInput.MicVolume;
        return detectionModifyer;
    }

    protected void Patrol() {
        agent.SetDestination(PatrolPoints[patrolIndex].position);

        if (agent.remainingDistance <= 0 && !IndexUpdted) {
            patrolIndex++;
            IndexUpdted = true;
            StartCoroutine(PatrolDelay());
        }

        if (patrolIndex > PatrolPoints.Length - 1) {
            patrolIndex = 0;
        }
            
    }

    protected IEnumerator PatrolDelay() {
        yield return patrolDelay;
        IndexUpdted = false;
        yield return null;
    }
    

}
