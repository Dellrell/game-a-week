﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDetector : MonoBehaviour {

    private int keyMask = 1 << 10;
    private bool keyHit;

	void Update () {
	    keyHit = Physics.Raycast(transform.position, transform.forward, 2, keyMask);
        Debug.DrawRay(transform.position, transform.forward, Color.yellow);
        if(keyHit) {GameManager.Instance.WinGame();}
	}
}
