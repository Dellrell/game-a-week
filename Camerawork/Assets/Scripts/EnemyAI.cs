﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyAI : AbstractAI {

    private bool playerHit;
    private float fov = 110;
    private float angleToPlayer;
    private RaycastHit hit;
    private GameManager gameManager;

    private void Start () {
	    agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(PatrolPoints[patrolIndex].position);
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    private void Update () {
        Vector3 rayDir = Vector3.Normalize(player.position - transform.position);
        playerHit = Physics.Raycast(transform.position, rayDir, out hit, 10) && hit.transform.gameObject.layer == 11;
        Debug.DrawRay(transform.position, rayDir * 10, Color.blue);

        angleToPlayer = Vector3.Angle(rayDir, transform.forward);

        Patrol();

        if (playerHit && !gameManager.playerHidden) {
            if (angleToPlayer < fov*0.5f) {
                gameManager.LoseGame();
            }
            
        }
        
    }
}
