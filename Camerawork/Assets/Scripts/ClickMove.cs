﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(NavMeshAgent))]
public class ClickMove : MonoBehaviour {
    public Transform switchHandle;
    public Transform securityBars;
    public GameObject diamond;

    private Vector3 destinationPos;
    public NavMeshAgent agent;
    private Renderer thisRenderer;
    
    private CameraSwitch cameraSwitch;
    private RaycastHit gHit;
    private RaycastHit iHit;
    
    private GameManager gameManager;

    private bool groundHit;
    private bool interactableHit;
    private Camera activeCamera;

    void Start() {
        agent = GetComponent<NavMeshAgent>();
        cameraSwitch = FindObjectOfType<CameraSwitch>();
        thisRenderer = GetComponent<Renderer>();
        gameManager = FindObjectOfType<GameManager>();

    }

    private void Update() {
        if (cameraSwitch.activeCamera != null)
            activeCamera = cameraSwitch.activeCamera;

        if(activeCamera != null)
        {
            Ray pointerRay = activeCamera.ScreenPointToRay(Input.mousePosition);
             groundHit = Physics.Raycast(pointerRay, out gHit, Mathf.Infinity) && gHit.transform.gameObject.layer == 9;
             interactableHit = Physics.Raycast(pointerRay, out iHit, Mathf.Infinity) && iHit.transform.gameObject.layer == 10;
        }
       

        if (groundHit && Input.GetButtonUp("Fire1")) {
            OnGroundClick();
        }

        if (interactableHit && Input.GetButtonUp("Fire1")) {
            destinationPos = iHit.transform.GetChild(0).position;
            agent.SetDestination(destinationPos);
            if (agent.remainingDistance < 1) {
                OninteractableHit();
            }
            
        }
    }

    private void OnGroundClick() {

        if (agent.pathPending || !thisRenderer.isVisible)
            return;
        
        destinationPos = gHit.point;
        
        agent.SetDestination(destinationPos);
    }

    private void OninteractableHit() {
        if (agent.pathPending || !thisRenderer.isVisible)
            return;

        
        if (iHit.transform.tag == "Switch") {
            gameManager.securityOff = true;
            Quaternion targetTrans = Quaternion.Euler(0, -31.275f, 117);
            switchHandle.rotation = targetTrans;
              
            Vector3 targetPos = new Vector3(14.15697f, -12.05f, 0.2781297f);
            securityBars.position = targetPos;


        } else if (iHit.transform.tag == "Diamond" && gameManager.securityOff) {
            diamond.SetActive(false);
            gameManager.diamondGot = true;

        } else if (iHit.transform.tag == "Window" && gameManager.diamondGot) {
            gameManager.WinGame();
        }
    }

    private void OnTriggerEnter(Collider collider) {
        if (collider.tag == "Hiding") {
            //print("hidden");
            gameManager.playerHidden = true;
        }
    }

    private void OnTriggerExit(Collider collider) {
        if (collider.tag == "Hiding") {
            //print("unhidden");
            gameManager.playerHidden = false;
        }
    }
}
