﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour {

    public Camera[] securityCameras;
    public Camera activeCamera;
   
	
	void Update () {

	    if (Input.GetKeyDown(KeyCode.Alpha1)) {
            SwitchCameraOne();
	    }
	    if (Input.GetKeyDown(KeyCode.Alpha2)) {
	        SwitchCameraTwo();
	    }
	    if (Input.GetKeyDown(KeyCode.Alpha3)) {
	       SwitchCameraThree();
	    }
	    if (Input.GetKeyDown(KeyCode.Alpha4)) {
	       SwitchCameraFour();
	    }
	    if (Input.GetKeyDown(KeyCode.Alpha5)) {
	       SwitchCameraFive();
	    }
	    if (Input.GetKeyDown(KeyCode.Alpha6)) {
	       SwitchCameraSix();
	    }
	}
    public void SwitchCameraOne() {
        if (activeCamera != null)
            activeCamera.gameObject.SetActive(false);

        activeCamera = securityCameras[0];
        activeCamera.gameObject.SetActive(true);
    }

    public void SwitchCameraTwo() {
        if (activeCamera != null)
            activeCamera.gameObject.SetActive(false);

        activeCamera = securityCameras[1];
        activeCamera.gameObject.SetActive(true);
    }

    public void SwitchCameraThree() {
        if (activeCamera != null)
            activeCamera.gameObject.SetActive(false);

        activeCamera = securityCameras[2];
        activeCamera.gameObject.SetActive(true);
    }

    public void SwitchCameraFour() {
        if (activeCamera != null)
            activeCamera.gameObject.SetActive(false);

        activeCamera = securityCameras[3];
        activeCamera.gameObject.SetActive(true);
    }

    public void SwitchCameraFive() {
        if (activeCamera != null)
            activeCamera.gameObject.SetActive(false);

        activeCamera = securityCameras[4];
        activeCamera.gameObject.SetActive(true);
    }

    public void SwitchCameraSix() {
        if (activeCamera != null)
            activeCamera.gameObject.SetActive(false);

        activeCamera = securityCameras[5];
        activeCamera.gameObject.SetActive(true);
    }


}
