﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof( NavMeshAgent))]
public abstract class AbstractAI : MonoBehaviour {

    public Transform[] PatrolPoints;
    public Transform player;
    
    protected NavMeshAgent agent;
    
    protected int patrolIndex = 0;
    private readonly WaitForSeconds patrolDelay = new WaitForSeconds(5);
    protected bool IndexUpdted;
    
    protected void Patrol() {
        
        if (agent.remainingDistance <= 0 && !IndexUpdted) {
            patrolIndex++;
            IndexUpdted = true;
            StartCoroutine(PatrolDelay());
        }

        if (patrolIndex > PatrolPoints.Length - 1) {
            patrolIndex = 0;
        }

        if (IndexUpdted) {
            transform.Rotate(0, 50 * Time.deltaTime, 0, Space.Self);
        }
            
    }


    protected IEnumerator PatrolDelay() {
        yield return patrolDelay;
        agent.SetDestination(PatrolPoints[patrolIndex].position);
        IndexUpdted = false;
    }
    

}
