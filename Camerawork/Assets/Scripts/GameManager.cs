﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public bool gameIsPaused;
    public GameObject pauseScreen;

    public GameObject startScreen;
    public GameObject loseScreen;
    public GameObject winScreen;
    
    public bool playerHidden;
    public bool securityOff;
    public bool diamondGot;

    private CameraSwitch cameraSwitch;
    private AudioSource audioSource;
    public AudioClip Alarm;
    public AudioClip Chaching;
    
	void Start () {
	   
	    cameraSwitch = FindObjectOfType<CameraSwitch>();
	    audioSource = GetComponent<AudioSource>();
	    Time.timeScale = 0;
	}

    void Update() {
        if(Input.GetKeyDown(KeyCode.Escape))
            PauseMenu();
    }

    public void WinGame() {
        if (!winScreen.activeSelf) {
            winScreen.SetActive(true);
            audioSource.PlayOneShot(Chaching);
            Time.timeScale = 0;
        }


    }

    public void LoseGame() {
        if (!loseScreen.activeSelf) {
            loseScreen.SetActive(true);
            audioSource.PlayOneShot(Alarm);
            Time.timeScale = 0;
        }

    }

    public void PauseMenu() {
        gameIsPaused = !gameIsPaused;
        if (gameIsPaused) {
            pauseScreen.SetActive(true);
            Time.timeScale = 0;
        }
        else {
            pauseScreen.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void StartGame() {
        startScreen.SetActive(false);
        cameraSwitch.SwitchCameraOne();
        Time.timeScale = 1;

    }

    public void QuitGame() {
        Application.Quit();
    }

    public void RestartGame() {
        gameIsPaused = false;
        pauseScreen.SetActive(false);
        winScreen.SetActive(false);
        loseScreen.SetActive(false);
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }


}
