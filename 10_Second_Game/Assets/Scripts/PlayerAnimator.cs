﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour {

    public RuntimeAnimatorController Controller0;
    public RuntimeAnimatorController Controller1;
    public RuntimeAnimatorController Controller2;
    public RuntimeAnimatorController Controller3;

    private Animator animator;
    private PlayerInteract playerInteract;

    private readonly int hashXDirPara = Animator.StringToHash("xDir");
    private readonly int hashYDirPara = Animator.StringToHash("yDir");
    private int xDir;
    private int yDir;


	// Use this for initialization
	void Start () {
	    animator = GetComponent<Animator>();
	    playerInteract = GetComponent<PlayerInteract>();
	}
	
	// Update is called once per frame
	void Update () {

	    float xAxis = Input.GetAxisRaw("Horizontal");
	    float yAxis = Input.GetAxisRaw("Vertical");

	    if (xAxis > 0) {
	        xDir = 1;
	    }
	    else if (xAxis < 0) {
	        xDir = -1;
	    }
	    else {
	        xDir = 0;
	    }


	    if (yAxis > 0) {
	        yDir = 1;
	    }
	    else if (yAxis < 0) {
	        yDir = -1;
	    }
	    else {
	        yDir = 0;
	    }
        
        animator.SetInteger(hashXDirPara, xDir);
        animator.SetInteger(hashYDirPara, yDir);

	    switch (playerInteract.foundClothes) {
            case 1: animator.runtimeAnimatorController = Controller1; return;
            case 2: animator.runtimeAnimatorController = Controller2; return;
            case 3: animator.runtimeAnimatorController = Controller3; return;   
	        default: animator.runtimeAnimatorController = Controller0; return;
	    }

	     
	}
}
