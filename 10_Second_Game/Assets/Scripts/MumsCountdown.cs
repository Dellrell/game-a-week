﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MumsCountdown : MonoBehaviour {

    private float startTime;
    private float currentTimer;
    private float delta;
    public int displayTime;

    public PlayerInteract interact;
    public GameController controller;

	void OnEnable () {
	    startTime = Time.time;
	}
	

	void Update () {
	    currentTimer = Time.time;
	    delta = Mathf.Abs(startTime - currentTimer);
	    displayTime = Mathf.CeilToInt(delta);
        //Debug.Log(displayTime);

	    if (delta > 10) {
	        if (interact.foundClothes >= 3) {
                controller.SuccessGame();
	        }
	        else {
                controller.FailGame();
	        }
	    }
	}
}
