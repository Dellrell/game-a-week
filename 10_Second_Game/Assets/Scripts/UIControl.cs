﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour {
    
    public enum Boxes {
        box1,
        box2,
        box3
    }

    public Boxes box;
    public Sprite found;
    public PlayerInteract interact;

    private Image image;

	// Use this for initialization
	void Start () {
	    image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (box == Boxes.box1) {
	        if (interact.foundClothes == 1) {
	            image.sprite = found;
	        }
	    }

	    if (box == Boxes.box2) {
	        if (interact.foundClothes == 2) {
	            image.sprite = found;
	        }
	    }

	    if (box == Boxes.box3) {
	        if (interact.foundClothes >= 3) {
	            image.sprite = found;
	        }
	    }
	}
}
