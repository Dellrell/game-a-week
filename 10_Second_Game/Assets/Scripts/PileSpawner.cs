﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PileSpawner : MonoBehaviour {

    public GameObject pilePrefab0;
    public GameObject pilePrefab1;
    public GameObject pilePrefab2;
    public GameObject pilePrefab3;
    private Grid grid;

    public int maxPiles = 10;
    public int ColumnNum = 8;
    public int RowNum = 6;
    
    private int index;
    private int pindex;

    private List<Vector3Int> allGridPos;
    private List<Vector3Int> randomPositions;
    private List<GameObject> piles;
    private List<GameObject> fullPiles;
    

	// Use this for initialization
	void Start () {
	    grid = GetComponent<Grid>();
        allGridPos = new List<Vector3Int>();
        randomPositions = new List<Vector3Int>();
        piles = new List<GameObject> {pilePrefab0, pilePrefab1, pilePrefab2, pilePrefab3};
        fullPiles = new List<GameObject>();

        CreateGridPos(ColumnNum, RowNum, out allGridPos);
        
        //make a list of random positions to check against so there're no repeats 
	    while (randomPositions.Count < maxPiles) {
	        index = (int)Mathf.Round(Random.Range(0, allGridPos.Count));

            if(index != 42 && index != 43 && !randomPositions.Contains(allGridPos[index]))
                randomPositions.Add(allGridPos[index]);
	    }

	    foreach (Vector3Int position in randomPositions) {
	        Vector3 worldPos = grid.CellToWorld(position);
	        pindex = (int) Mathf.Round(Random.Range(0, 4));
	        //add the first three to "full piles"
	        if (fullPiles.Count < 3) {
                fullPiles.Add(Instantiate(piles[pindex], worldPos, Quaternion.identity));
	        }
	        else { //just instantiate the rest
	            Instantiate(piles[pindex], worldPos, Quaternion.identity);
	        }
	    }

	    foreach (GameObject pile in fullPiles) {
	        pile.tag = "full";
	    }
	}

    private void CreateGridPos(int theNumCol, int theNumRow, out List<Vector3Int> gridPos) {

        //create collection to store raw open positions
        List<Vector3Int> allPos = new List<Vector3Int>();

        //create grid of positions from 0,1 0,2... 1,0 2,0... 1,1, 1,2...etc
        //iterate through columns
        for (int col = 0; col < theNumCol; col++) {
            //iterate through rows
            for (int row = 0; row < theNumRow; row++) {
                //store position
                Vector3Int pos = new Vector3Int(col, row, 0);
                //add position
                allPos.Add(pos);
            }
        }
        gridPos =  allPos;
    }
}
