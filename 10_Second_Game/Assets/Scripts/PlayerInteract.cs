﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour {

    public int foundClothes = 0;

    private Vector2 playerDir;
    private Rigidbody2D rb;
    private bool hit;
    private Vector2 playerVelocity;
    private int layerMask = 1 << 8;
    private Vector2 lastDir;

    private int xDir;
    private int yDir;

    private RaycastHit2D rayHit;
    private SpriteRenderer spRend;
    private ContainerSprites csprites;
    private Sprite openSprite;

    private AudioSource ding;

	// Use this for initialization
	void Start () {

	    rb = GetComponent<Rigidbody2D>();
        //set initial value for player direction 
        playerDir = Vector2.down;
	    ding = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

	    xDir = (int)Input.GetAxisRaw("Horizontal");
	    yDir = (int) Input.GetAxisRaw("Vertical");
	    playerVelocity = new Vector2(xDir, yDir);

	    if (xDir != 0 || yDir != 0) {

	        lastDir = playerVelocity;
	        playerDir = playerVelocity;

	    } else if (xDir == 0 && yDir == 0) {

	        playerDir = lastDir;

	    }

	    rayHit = Physics2D.Raycast(rb.position, playerDir, 1, layerMask);
        Debug.DrawRay(transform.position, playerDir, Color.yellow);

	    if (rayHit) {
	        spRend = rayHit.transform.gameObject.GetComponent<SpriteRenderer>();
	        csprites = rayHit.transform.gameObject.GetComponent<ContainerSprites>();
	        openSprite = csprites.openSprite;

	        if (Input.GetKeyDown(KeyCode.Space)) {
	            if (spRend.sprite != openSprite)
	                spRend.sprite = openSprite;

	            if (rayHit.transform.tag == "full") {
	                foundClothes++;
	                rayHit.transform.tag = "Untagged";
                    
                    ding.Play();
	                
	            }//end clothes if

	        }//end "space" if

	    }//end rayhit if
        
	}//end void update 
}
