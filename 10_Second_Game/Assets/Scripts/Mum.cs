﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mum : MonoBehaviour {

    public Sprite[] shadows;

    private SpriteRenderer shadow;
    private MumsCountdown countdown;

	// Use this for initialization
	void Start () {
	    shadow = GetComponent<SpriteRenderer>();
	    countdown = GetComponent<MumsCountdown>();
	}
	
	// Update is called once per frame
	void Update () {
	    int index = countdown.displayTime - 1;
	    if (index >= 0 && index <= 9) {
	        shadow.sprite = shadows[index];
	    }
	}
}
