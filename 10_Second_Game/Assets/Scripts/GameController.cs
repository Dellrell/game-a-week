﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public GameObject theCountdown;
    public GameObject timer;
    public GameObject start;
    public GameObject fail;
    public GameObject success;
    public GameObject pileSpawner;
    public PlayerMovement playerMove;
    public PlayerAnimator playerAnim;

    public AudioSource timerMusic;
    public AudioSource failMusic;
    public AudioSource successMusic;
    public AudioSource menuMusic;

    private int currentState = 0;

    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {

            if (currentState == 0) {

                StartGame();

            } else if (currentState == 2) {

                ReplayGame();
            }
        }
    }

    public void StartGame() {
        start.SetActive(false);
        theCountdown.SetActive(true);
        timer.SetActive(true);
        pileSpawner.SetActive(true);
        playerMove.enabled = true;
        playerAnim.enabled = true;
        currentState = 1;

        timerMusic.Play();
        menuMusic.Stop();
        
    }

    public void FailGame() {
        theCountdown.SetActive(false);
        timer.SetActive(false);
        pileSpawner.SetActive(false);
        fail.SetActive(true);
        playerMove.enabled = false;
        playerAnim.enabled = false;
        currentState = 2;

        failMusic.Play();
    }

    public void SuccessGame() {
        theCountdown.SetActive(false);
        timer.SetActive(false);
        pileSpawner.SetActive(false);
        success.SetActive(true);
        playerMove.enabled = false;
        playerAnim.enabled = false;
        currentState = 2;

        successMusic.Play();
    }

    public void ReplayGame() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        currentState = 0;
    }

    public void QuitGame() {
        Application.Quit();
        Debug.Log("Quit");
    }
}
