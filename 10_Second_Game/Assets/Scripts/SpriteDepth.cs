﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteDepth : MonoBehaviour {
    
    private Transform spritePos;

	// Use this for initialization
	void Start () {

	    spritePos = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {

        //set sprite depth 
        spritePos.position = new Vector3(spritePos.position.x, spritePos.position.y, spritePos.position.y);
	}
}
