﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownDisplay : MonoBehaviour {

    public Sprite[] numbers;
    public MumsCountdown Countdown;

    private Image number;

	// Use this for initialization
	void Start () {
	    number = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
	    int index = Countdown.displayTime - 1;
	    if (index >= 0 && index <= 9) {
	        number.sprite = numbers[index];
	    }
	    
	}
}
