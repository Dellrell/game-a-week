﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sing : MonoBehaviour {

    public GameObject TrailFinder;
    public GameObject SoundWaveObj;
    public static int NumTrailFinders;

    private AudioSource singAudio;

    void Start() {
        singAudio = GetComponent<AudioSource>();
    }

	void Update () {

	    if (Input.GetKeyDown(KeyCode.Space) && NumTrailFinders < 1) {
	        Instantiate(TrailFinder, transform.position, Quaternion.identity);
	        Instantiate(SoundWaveObj, transform.localPosition, transform.localRotation, transform);
            singAudio.Play();
	        NumTrailFinders++;
	    }
	}
}
