﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class PlayerMovement : MonoBehaviour {

    public EndGame endgame;

    public float speed = 5.0f;
    public float gravity = 10.0f;
    private float horizontalInput;
    private float verticalInput;
    private CharacterController controller;
    private Vector3 movement = Vector3.zero;


	// Use this for initialization
	void Start () {
	    controller = GetComponent<CharacterController>();
	}
	
	void FixedUpdate () {
        //get input
	    horizontalInput = Input.GetAxis("Horizontal");
	    verticalInput = Input.GetAxis("Vertical");

	    if (controller.isGrounded) {
            //apply input
	        movement = new Vector3(horizontalInput, 0, verticalInput);
            movement = transform.TransformDirection(movement) * speed;

	    }

	    //apply gravity
	    movement.y -= gravity * Time.deltaTime;
	    //move controller
	    controller.Move(movement * Time.deltaTime);

        if(transform.position.y < -3)
            endgame.EndTheGame();
    }
}
