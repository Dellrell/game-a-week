﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockMouse : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    Cursor.lockState = CursorLockMode.Locked;
	    Cursor.visible = false;

	    if (Input.GetKey(KeyCode.Escape)) {
	        Cursor.lockState = CursorLockMode.None;
	        Cursor.visible = true;
	    }
	}
}
