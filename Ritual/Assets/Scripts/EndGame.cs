﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGame : MonoBehaviour {

    public Image FadeOut;
    private bool end;

    private float timer = 0;
    private float endTimer = 1.5f;
	
	// Update is called once per frame
	void Update () {

	    if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
	    }

	    if (end) {

	        timer += Time.deltaTime;

	        FadeOut.color = Color.Lerp(FadeOut.color, Color.white, timer / endTimer);

	        if (timer >= endTimer) {
                EndTheGame();
	        }

	    }
        
	}

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            //Debug.Log("collide");
            end = true;
        
        }
            
    }

    public void EndTheGame() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
