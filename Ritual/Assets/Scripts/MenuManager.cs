﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    public Image Title;
    public Image StartButton;
    public PlayerMovement playerMove;
    public MouseLook playerLook;
    public MouseLook camerLook;

    private float rCol;
	
	// Update is called once per frame
	void Update () {

	    if (Input.GetKeyDown(KeyCode.Space)) {
            StartGame();
	    }

	    rCol = 0.5f * Mathf.Sin(Time.time) + 1f;
	    Color buttonCol = new Color(rCol, 0.18f, 0.69f, 1);
       
	    StartButton.color = buttonCol;
	}

    void StartGame() {
        Title.gameObject.SetActive(false);
        StartButton.gameObject.SetActive(false);
        playerMove.enabled = true;
        playerLook.enabled = true;
        camerLook.enabled = true;
    }
}
