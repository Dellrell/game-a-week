﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using GameObject = UnityEngine.GameObject;

public class TrailFinder : MonoBehaviour {

    private Transform target;
    private NavMeshAgent agent;
    private float lTimer;

	// Use this for initialization
	void Awake () {
	    agent = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Goal").transform;
	}
	
	// Update is called once per frame
	void Update () {

	    agent.SetDestination(target.position);

	    lTimer += Time.deltaTime;
        

	    if (lTimer >= 4.5f) 
            agent.isStopped = true;

	    if (lTimer >= 6.0f) {
	        Destroy(this.gameObject);
	        Sing.NumTrailFinders--;
	    }
           
	}
}
