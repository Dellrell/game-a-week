﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseLook : MonoBehaviour {

    public enum lookAxis { xAxis, yAxis};

    public lookAxis axis = lookAxis.xAxis;

    public float xSensitivity;
    public float ySensitivity;
    public bool invertY;

    //public Slider sensitivitySlider;
    //public Toggle YToggle;

    private int invertFlag;

    public float minimumY = -80.0f;
    public float maximumY = 80.0f;
    
    private float hRotation;
    private float vRotation;
    private Quaternion initialRotation;

    private List<float> hRotArray = new List<float>();
    private float hRotAverage;
    private List<float> vRotArray = new List<float>();
    private float vRotAverage;
    public float framesOfSmoothing = 5;

	// Use this for initialization
	void Start () {
	    initialRotation = transform.localRotation;
	}
	
	void FixedUpdate () {

	    //xSensitivity = sensitivitySlider.value;
	    //ySensitivity = sensitivitySlider.value;
	    //invertY = YToggle.isOn;

	    if (axis == lookAxis.xAxis) {
	        hRotAverage = 0;

	        hRotation += Input.GetAxis("Mouse X") * xSensitivity;
            
            hRotArray.Add(hRotation);

	        if (hRotArray.Count >= framesOfSmoothing) {
                hRotArray.RemoveAt(0);
	        }

	        foreach (float i in hRotArray) {
	            hRotAverage += i;
	        }
	        hRotAverage /= hRotArray.Count;
            
            Quaternion xQuaternion = Quaternion.AngleAxis(hRotAverage, Vector3.up);
	        transform.localRotation = initialRotation * xQuaternion;
	    }
	    else {
	        vRotAverage = 0;
            invertFlag = invertY ? -1 : 1;

            vRotation += Input.GetAxis(("Mouse Y")) * ySensitivity * invertFlag;
	        vRotation = Mathf.Clamp(vRotation, minimumY, maximumY);

            vRotArray.Add(vRotation);
	        if (vRotArray.Count >= framesOfSmoothing) {
                vRotArray.RemoveAt(0);
	        }

	        foreach (float j in vRotArray) {
	            vRotAverage += j;
	        }
	        vRotAverage /= vRotArray.Count;
            
	        Quaternion yQuaternion = Quaternion.AngleAxis(vRotAverage, Vector3.left);
	        transform.localRotation = initialRotation * yQuaternion;
	    }
	}
}


