﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTrigger : MonoBehaviour {

    public GameObject player;
    private PlayerMovement playerMovement;
    private Rigidbody playeRigidbody;
    private CharacterController playerController;

    public Rigidbody[] FurnitureRigidbodies;

    public Image fadeOutPanel;

    private float timer;
    private float restartTimer;

    private GameManager gameManager;

    
	void Start () {
	    playerMovement = player.GetComponent<PlayerMovement>();
	    playeRigidbody = player.GetComponent<Rigidbody>();
	    playerController = player.GetComponent<CharacterController>();
	    gameManager = FindObjectOfType<GameManager>();
	}
	
	void Update () {
	    if (timer > 8) {
	        foreach (Rigidbody rb in FurnitureRigidbodies) {
	            rb.isKinematic = false;
	        }
	    }

	    if (timer > 10) {
	        playerMovement.enabled = false;
	        playerController.enabled = false;
	        playeRigidbody.isKinematic = false;

	       
	        fadeOutPanel.color = Color.Lerp(fadeOutPanel.color, Color.black, 0.01f);

	        restartTimer += Time.deltaTime;
	        if (restartTimer >= 4) {
	            gameManager.RestartGame();
	        }
	    }
	  
	}

    void OnTriggerStay(Collider other) {
        if (other.tag == "Player") {
            timer += Time.deltaTime;
        }
    }
}
