﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour {

    public bool open;

    private OpenDoors openDoors;
    private float timer;
    
    private void Start() {
        openDoors = FindObjectOfType<OpenDoors>();
    }

    private void Update() {

        timer = open ? timer += Time.deltaTime : 0;

        if (open) {
            if (timer >= 3) {
                StartCoroutine(openDoors.DoorSwing(Quaternion.identity, transform, this, 2.0f));
                open = !open;
            }
        }
    }
}
