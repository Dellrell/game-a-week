﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTrigger : MonoBehaviour {

    public GameObject levelToSwitchOff;
    public GameObject levelToSwitchOn;

    public GameObject playerLookChecker;
    private LookCheck check;

    public GameObject[] associatedDoors;
    private DoorManager[] doorManagers;

    private bool levelReady;

    private void Start() {
        if (playerLookChecker != null)
            check = playerLookChecker.GetComponent<LookCheck>();
       
        doorManagers = new DoorManager[associatedDoors.Length];

        for (int i = 0; i < associatedDoors.Length; i++) {
            doorManagers[i] = associatedDoors[i].GetComponent<DoorManager>();
        }
    }

    void Update() {
        
        if (associatedDoors.Length <= 0 && playerLookChecker == null) {
            levelReady = true;
        } else if (associatedDoors.Length > 0 && playerLookChecker == null) {
            if(CheckDoors())
                levelReady = true;
        } else if (playerLookChecker != null && associatedDoors.Length <= 0) {
            if(check.playerLooking)
                levelReady = true;
        } else {
            levelReady = false;
        }

    }

    private bool CheckDoors() {
        foreach (var door in doorManagers) {
            if (door.open)
                return false;
        }
        return true;
    }

    private IEnumerator SwitchLevels() {
        yield return new WaitForEndOfFrame();
        levelToSwitchOn.SetActive(true);
        levelToSwitchOff.SetActive(false);
    }

    void OnTriggerStay(Collider other) {
        if (other.tag == "Player" && levelReady)
            StartCoroutine(SwitchLevels());
    }
}
