﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutscenes : MonoBehaviour {

    public Transform Player;
    public bool Begin;

    public IEnumerator StartCutscene(float duration) {
        float timer = 0;
        while (timer < duration) {
            timer += Time.deltaTime;
            Player.position = Vector3.Lerp(Player.position, new Vector3(-1.0f, Player.position.y, Player.position.z), timer / duration);
            yield return null;
        }

        timer = 0;
        while (timer < duration) {
            timer += Time.deltaTime;
            Player.position = Vector3.Lerp(Player.position, new Vector3(-1.0f, 0.49f, Player.position.z), timer / duration);
            yield return null;
        }

        Begin = true;

    }
}
