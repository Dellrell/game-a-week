﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RainbowInstructions : MonoBehaviour {
    
    public Color[] Colours;

    private Outline outline;
    private float timer;
    private int index;

	private void Start () {
	    outline = GetComponent<Outline>();
	}
	
	private void Update () {
	    timer += Time.deltaTime;
	    if (timer > 0.1f) {
            UpdateColour();
	    }
	}

    private void UpdateColour() {
        timer = 0;
        index++;
        if (index > Colours.Length -1)
            index = 0;
        outline.effectColor = Colours[index];
    }
}
