﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public AudioClip Fail;
    public AudioClip TotalFail;
    public AudioClip Success;
    public AudioClip MenuMusic;
    public AudioClip GameMusic;

    private AudioSource source;

    private void Awake() {
        source = GetComponent<AudioSource>();
    }

    private void OnEnable() {
        MicrogameManager.OnSuccess += PlaySuccess;
        MicrogameManager.OnFailure += PlayFailure;
        MicrogameManager.OnTotalFailure += PlayTotalFail;
        ButtonManager.OnStartGame += PlayGameMusic;

    }

    private void OnDisable() {
        MicrogameManager.OnSuccess -= PlaySuccess;
        MicrogameManager.OnFailure -= PlayFailure;
        MicrogameManager.OnTotalFailure -= PlayTotalFail;
        ButtonManager.OnStartGame -= PlayGameMusic;
    }

    private void PlayFailure() {
        source.PlayOneShot(Fail);
    }

    private void PlayTotalFail() {
        source.PlayOneShot(TotalFail);
    }

    private void PlaySuccess() {
        source.PlayOneShot(Success);
    }

    private void PlayGameMusic() {
        source.clip = GameMusic;
        source.Play();
    }

}
