﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {
    public delegate void StartGame();
    public static event StartGame OnStartGame;

    public GameObject StartScreen;

    private Cutscenes cutscenes;

    private void Awake() {
        cutscenes = FindObjectOfType<Cutscenes>();
    }

    public void RestartTheGame() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void QuitTheGame() {
        Application.Quit();
    }

    public void StartTheGame() {
        StartScreen.SetActive(false);
        StartCoroutine(cutscenes.StartCutscene(2.5f));
        if(OnStartGame != null){OnStartGame();}
    }
}
