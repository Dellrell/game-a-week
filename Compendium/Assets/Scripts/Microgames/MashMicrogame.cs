﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MashMicrogame : MonoBehaviour {

    public Slider Slider;
    public Text Counter;

    public GameObject Game;
    public GameObject Instructions;
    private float introTimer;

    private float microgameTimer;
    private MicrogameManager manager;
    private int difficulty;
    private float fill;

	private void Awake () {
	    manager = FindObjectOfType<MicrogameManager>();
	    difficulty = 1;
	}

    private void OnEnable() {
        fill = 0;
        microgameTimer = 5;
        difficulty = manager.Difficulty;
        Game.SetActive(false);
        Instructions.SetActive(true);
        introTimer = 0;
    }

	void Update () {
	    introTimer += Time.deltaTime;
	    if (introTimer >= 2) {
	        Game.SetActive(true);
	        Instructions.SetActive(false);
	    }

	    if (Game.activeSelf) {
	        microgameTimer -= Time.deltaTime;
	        Counter.text = Mathf.Round(microgameTimer).ToString();
	        if (microgameTimer <= 0) {
	            manager.EndMicrogame(manager.ActiveIndex, false);
	        }
	    }

	    fill -= (Time.deltaTime + (difficulty * 0.01f));
	    fill = Mathf.Clamp(fill, Slider.minValue, Slider.maxValue);
	    if (Input.GetKeyDown(KeyCode.Space)) {
	        fill += 0.5f;
	    }

	    Slider.value = fill;

	    if (fill >= Slider.maxValue) {
           manager.EndMicrogame(manager.ActiveIndex, true);
	    }
	}
}
