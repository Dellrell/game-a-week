﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MicrogameManager : MonoBehaviour {
    public delegate void Success();
    public static event Success OnSuccess;

    public delegate void Failure();
    public static event Failure OnFailure;

    public delegate void TotalFailure();
    public static event TotalFailure OnTotalFailure;

    public int Difficulty;
    public int Failures;
    public int Wins;
    public int ActiveIndex;

    public GameObject[] Minigames;
    public Image[] Spoons;
    public Text Score;
    public Sprite BadSpoon;

    public GameObject EndScreen;
    public Text FinalScore;

    private Cutscenes cutscenes;
    private float mgTimer;
    private bool inGame;


	void Awake () {
	    Difficulty = 1;
	    cutscenes = FindObjectOfType<Cutscenes>();
	}
	

	void Update () {
        if(!cutscenes.Begin){return;}

	    mgTimer += Time.deltaTime;
	    if (mgTimer >= 3 && !inGame) {
	        inGame = true;
	        ActiveIndex = Random.Range(0, 3);
	        StartMicrogame(ActiveIndex);
	    }
	}

    private void StartMicrogame(int index) {
        Minigames[index].SetActive(true);
    }

    public void EndMicrogame(int index, bool win) {
        mgTimer = 0;
        Minigames[index].SetActive(false);
        if (!win) {
            if(OnFailure != null){OnFailure();}
            Failures++;
            if(Failures > Spoons.Length){Failures = Spoons.Length;}
            Spoons[Failures - 1].sprite = BadSpoon;
            Spoons[Failures - 1].GetComponent<UIAnimation>().Disabled = true;
            if (Failures == Spoons.Length) {
                if(OnTotalFailure != null){OnTotalFailure();}
                LoseGame();
            }
        }
        else {
            Wins++;
            if(OnSuccess != null){OnSuccess();}
            Score.text = Wins.ToString();
            if (Wins % 5 == 0) {
                Difficulty++;
                if (Difficulty > 3) {
                    Difficulty = 3;
                }
            }
        }

        inGame = false;
    }

    private void LoseGame() {
        EndScreen.SetActive(true);
        FinalScore.text = "Score: " + Wins;
    }
}
