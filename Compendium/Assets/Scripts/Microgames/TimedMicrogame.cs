﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimedMicrogame : MonoBehaviour {

    public RectTransform Leg;
    public RectTransform Shoe;
    public Text Counter;

    public GameObject Game;
    public GameObject Instructions;
    private float introTimer;

    private float microgameTimer;
    private float difficulty;
    private bool stopped;
    private MicrogameManager manager;

    private void Awake() {
        manager = FindObjectOfType<MicrogameManager>();
        difficulty = 1;
    }

    private void OnEnable () {
		Shoe.localPosition = new Vector3(Random.Range(-800, 800), -350, 0);
        Leg.localPosition = new Vector3(0, 550, 0);
        difficulty = manager.Difficulty;
        microgameTimer = 5;
        stopped = false;
        Game.SetActive(false);
        Instructions.SetActive(true);
        introTimer = 0;
    }

    private void Update () {
        introTimer += Time.deltaTime;
        if (introTimer >= 2) {
            Game.SetActive(true);
            Instructions.SetActive(false);
        }

        if (Game.activeSelf) {
            microgameTimer -= Time.deltaTime;
            Counter.text = Mathf.Round(microgameTimer).ToString();
            if (microgameTimer <= 0) {
                manager.EndMicrogame(manager.ActiveIndex, false);
            }
        }

        if(!stopped)
		    Leg.localPosition = new Vector3(Mathf.Sin(Time.time*difficulty)*800, 550, 0);

        if (Input.GetKeyDown(KeyCode.Space)) {
            StopLeg();
            //print(StopLeg());
            StartCoroutine(LowerLeg(3));
        }
    }

    private bool StopLeg() {
        stopped = true;
        Leg.localPosition = Leg.localPosition;
        return Leg.localPosition.x > Shoe.localPosition.x - 150 && Leg.localPosition.x < Shoe.localPosition.x + 150;
    }

    private IEnumerator LowerLeg(float duration) {
        float timer = 0;
        while (timer < duration) {
            timer += Time.deltaTime;
            Leg.localPosition = Vector3.Lerp(Leg.localPosition, new Vector3(Leg.localPosition.x, 100, 0), timer / duration);
            yield return null;
        }
        
        manager.EndMicrogame(manager.ActiveIndex, StopLeg());
    }
}
