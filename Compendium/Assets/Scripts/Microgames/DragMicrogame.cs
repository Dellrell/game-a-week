﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragMicrogame : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public RectTransform Leg;
    public RectTransform Sock;
    public RectTransform DropArea;
    public Text Counter;
    private Vector3 sockStartPos;
    private bool clickedSock;

    public GameObject Game;
    public GameObject Instructions;
    private float introTimer;

    private float microgameTimer;
    private MicrogameManager manager;
    private int difficulty;

    private void Awake() {
        manager = FindObjectOfType<MicrogameManager>();
        difficulty = 1;
    }

    private void OnEnable() {
        Leg.localPosition = new Vector3(-711, Random.Range(-250, 350), 0);
        Sock.localPosition = new Vector3(550, Random.Range(-250, 350), 0);
        sockStartPos = Sock.localPosition;
        microgameTimer = 5;
        difficulty = manager.Difficulty;
        Game.SetActive(false);
        Instructions.SetActive(true);
        introTimer = 0;
        switch (difficulty) {
            case 1:
                DropArea.localScale = new Vector3(2, 1.5f, 1);
                break;
            case 2:
                DropArea.localScale = new Vector3(2, 1, 1);
                break;
            case 3:
                DropArea.localScale = new Vector3(1, 1, 1);
                break;
            default:
                DropArea.localScale = new Vector3(1, 1, 1);
                break;

        }
    }

    private void Update() {
        introTimer += Time.deltaTime;
        if (introTimer >= 2) {
            Game.SetActive(true);
            Instructions.SetActive(false);
        }

        if (Game.activeSelf) {
            microgameTimer -= Time.deltaTime;
            Counter.text = Mathf.Round(microgameTimer).ToString();
            if (microgameTimer <= 0) {
                manager.EndMicrogame(manager.ActiveIndex, false);
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData) {
        clickedSock = eventData.pointerCurrentRaycast.gameObject == Sock.gameObject;
    }

    public void OnDrag(PointerEventData eventData) {
        if (!clickedSock) return;
        Sock.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData) {
        if (RectTransformUtility.RectangleContainsScreenPoint(DropArea, Input.mousePosition)) {
            Sock.position = DropArea.position;
            manager.EndMicrogame(manager.ActiveIndex, true);
        }
        else {
            Sock.localPosition = sockStartPos;
        }
    }
}
