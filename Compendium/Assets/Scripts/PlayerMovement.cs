﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [Range(1.0f, 10.0f)]
    public float Speed = 5.0f;

    private Transform player;
    private SpriteRenderer playerSprite;
    private Rigidbody2D rb;
    private Vector2 playerVelocity;

    private float spriteWidthOffset;
    private float spriteHeightOffset;

    private Vector3 screenBoundsMax;
    private Vector3 screenBoundsMin;
    private float screenRightEdge;
    private float screenLeftEdge;
    private float screenTopEdge;
    private float screenBottomEdge;


	// Use this for initialization
	void Start () {
	    player = GetComponent<Transform>();
	    playerSprite = GetComponent<SpriteRenderer>();
	    rb = GetComponent<Rigidbody2D>();
	    spriteWidthOffset = playerSprite.bounds.size.x / 2;
	    spriteHeightOffset = playerSprite.bounds.size.y;

	    //find the edges of the screen - **might need to recalculate later if player can change aspect ratio**
	    screenBoundsMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f));
	    screenBoundsMin = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, 0.0f));
        //set the boundaries for the player within the screen 
	    screenRightEdge = screenBoundsMax.x - spriteWidthOffset;
	    screenLeftEdge = screenBoundsMin.x + spriteWidthOffset;
	    screenTopEdge = screenBoundsMax.y - spriteHeightOffset;
	    screenBottomEdge = screenBoundsMin.y;
    }
	
	// Update is called once per frame
	void Update () {

	    playerVelocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * Time.deltaTime * Speed;

        //move player by moving rigidbody
	    rb.MovePosition(rb.position + playerVelocity);

        //restrict on the right
	    if (player.position.x > screenRightEdge)
	        player.position = new Vector3(screenRightEdge, player.position.y);
        //restrict on the left
        if (player.position.x < screenLeftEdge)
            player.position = new Vector3(screenLeftEdge, player.position.y);
        //restrict on the top
	    if (player.position.y > screenTopEdge)
	        player.position = new Vector3(player.position.x, screenTopEdge);
        //restrict on the bottom
	    if (player.position.y < screenBottomEdge)
	        player.position = new Vector3(player.position.x, screenBottomEdge);

	}
}
