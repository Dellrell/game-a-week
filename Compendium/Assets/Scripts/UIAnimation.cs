﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnimation : MonoBehaviour {

    public float Delay;
    public bool Disabled;
	
	void Update () {
        if (Disabled) {return;}
		transform.position += new Vector3(0, Mathf.Sin((Time.time - Delay)*6)*0.1f, 0);
	}
}
