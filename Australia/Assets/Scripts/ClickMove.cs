﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class ClickMove : MonoBehaviour {

    public Transform stairTop;
    public Transform stairBottom;

    private Vector3 destinationPos;
    public NavMeshAgent agent;

    private const float navMeshSampleDistance = 4.0f;

    void Start() {
        agent = GetComponent<NavMeshAgent>();
    }

    public void OnGroundClick(BaseEventData data) {

        if (agent.pathPending)
            return;

        PointerEventData pdata = (PointerEventData) data;
        NavMeshHit hit;

        destinationPos = NavMesh.SamplePosition(pdata.pointerCurrentRaycast.worldPosition, out hit, navMeshSampleDistance,
            NavMesh.AllAreas) ? hit.position : pdata.pointerCurrentRaycast.worldPosition;

        float agentSpeed = pdata.clickCount >= 2 ? 10.5f : 5.5f;

        agent.speed = agentSpeed;
        agent.SetDestination(destinationPos);
    }

    public void OnStairClick(BaseEventData data) {
        if (agent.pathPending)
            return;
        
        PointerEventData pdata = (PointerEventData) data;

        if (pdata.pointerCurrentRaycast.gameObject == stairTop.gameObject) 
            destinationPos = stairBottom.position;
        else if (pdata.pointerCurrentRaycast.gameObject == stairBottom.gameObject) 
            destinationPos = stairTop.position;
        
            

        float agentSpeed = pdata.clickCount >= 2 ? 10.5f : 5.5f;

        agent.speed = agentSpeed;
        agent.SetDestination(destinationPos);

    }
}
