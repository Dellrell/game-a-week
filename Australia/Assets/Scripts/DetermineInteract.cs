﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetermineInteract : MonoBehaviour {

    public Interactables interactable;

    private GameObject player;
    private PlayerInteract playerInteract;


    void Start() {
        player = GameObject.FindWithTag("Player");
        playerInteract = player.GetComponent<PlayerInteract>();
    }

    public void OnPointerClick() {
        playerInteract.EnterLookMode(interactable);
    }


}
