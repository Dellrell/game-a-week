﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform player;

    private Vector3 destination;
    private Vector3 offset = new Vector3(0, 5, 0);

	void Update () {
	    destination = player.position;

        if(MenuManager.gameStarted && !PlayerInteract.isInteracting)
	        transform.position = Vector3.Lerp(transform.position, destination + offset, 0.1f);
	}
}
