﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerManager : MonoBehaviour {

    public Texture2D footprints;
    public Texture2D stairs;
    public Texture2D hand;
    public Texture2D eye;
    public Texture2D bed;
    public Texture2D arrow;

	void Start () {
		Cursor.SetCursor(arrow, Vector2.zero, CursorMode.Auto);
	}

    public void SetToFootprints() {
        Cursor.SetCursor(footprints, Vector2.zero, CursorMode.Auto);
    }

    public void SetToStairs() {
        Cursor.SetCursor(stairs, Vector2.zero, CursorMode.Auto);
    }

    public void SetToEye() {
        Cursor.SetCursor(eye, Vector2.zero, CursorMode.Auto);
    }

    public void SetToHand() {
        Cursor.SetCursor(hand, Vector2.zero, CursorMode.Auto);
    }
    
    public void SetToBed() {
        Cursor.SetCursor(bed, Vector2.zero, CursorMode.Auto);
    }
    
    public void SetToArrow() {
        Cursor.SetCursor(arrow, Vector2.zero, CursorMode.Auto);
    }
}

