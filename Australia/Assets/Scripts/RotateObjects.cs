﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObjects : MonoBehaviour {

    private float InspectionSensitivity = 5;
    private Camera cam;

    void Start() {
        cam = Camera.main;
    }

    void OnMouseDrag() {
        float hRotation = 0;
        float vRotation = 0;
        hRotation += Input.GetAxis("Mouse X") * InspectionSensitivity;
        vRotation += Input.GetAxis("Mouse Y") * InspectionSensitivity;
        transform.Rotate(cam.transform.up, -hRotation, Space.World); 
        transform.Rotate(cam.transform.right, vRotation, Space.World);
    }
}
