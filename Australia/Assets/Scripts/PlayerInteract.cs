﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerInteract : MonoBehaviour {

    public static bool isInteracting;

    public GameObject CameraCanvas;
    public GameObject OverlayCanvas;

    public PointerManager pointermanager;

    private Interactables currentInteractable;
    private GameObject currentInteractableObj;
    private Text comment;

    private Vector3 insPos;
    Vector3 offset = new Vector3(0, -0.25f, -1);

    private ClickMove clickMove;

	// Use this for initialization
	void Start () {
	    clickMove = GetComponent<ClickMove>();
        comment = OverlayCanvas.GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	    insPos = Camera.main.ScreenToWorldPoint(Vector3.zero, Camera.MonoOrStereoscopicEye.Mono) + offset;
	}

    public void EnterLookMode(Interactables interactable) {
        clickMove.enabled = false;
        clickMove.agent.isStopped = true;
        isInteracting = true;
        CameraCanvas.SetActive(true);
        OverlayCanvas.SetActive(true);

        currentInteractable = interactable;
        comment.text = currentInteractable.comment;
        currentInteractableObj = Instantiate(currentInteractable.interactableObj, insPos, Quaternion.identity);
        currentInteractableObj.transform.localScale = currentInteractable.interactableScale;

        pointermanager.SetToHand();

    }

    public void ExitLookMode() {
        clickMove.enabled = true;
        clickMove.agent.isStopped = false;
        isInteracting = false;
        CameraCanvas.SetActive(false);
        OverlayCanvas.SetActive(false);
        comment.text = "";
        Destroy(currentInteractableObj);

        currentInteractable = null;
        pointermanager.SetToFootprints();
    }
}
