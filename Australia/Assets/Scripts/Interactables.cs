﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Interactables : ScriptableObject {

    public GameObject interactableObj;
    public string comment;
    public Vector3 interactableScale;


}
