﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public static bool gameStarted;

    public GameObject Facade;
    public GameObject StartMenu;
    public GameObject EndMenu;
    public GameObject credits;

    public void StartGame() {
        Facade.SetActive(false);
        StartMenu.SetActive(false);
        gameStarted = true;
    }

    public void ActivateBed() {
        EndMenu.SetActive(true);
    }

    public void DeActivateBed() {
        EndMenu.SetActive(false);
    }

    public void ResetGame() {
        gameStarted = false;
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void ActivateCredits() {
        StartMenu.SetActive(false);
        credits.SetActive(true);

    }

    public void DeactivateCredits() {
        credits.SetActive(false);
        StartMenu.SetActive(true);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
