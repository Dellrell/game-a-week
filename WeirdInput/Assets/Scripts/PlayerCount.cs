﻿using System.Collections.Generic;
using CUE.NET;
using CUE.NET.Brushes;
using CUE.NET.Devices.Generic;
using CUE.NET.Devices.Generic.Enums;
using CUE.NET.Devices.Keyboard;
using CUE.NET.Groups;
using UnityEngine;
using UnityEngine.SceneManagement;
using Image = UnityEngine.UI.Image;

public class PlayerCount : MonoBehaviour {

    public static int ThePlayerCount;

    public Image background;
    public Sprite[] sprites;
    private CorsairKeyboard keyboard;

    private ListLedGroup group1;
    private ListLedGroup group2;
    private ListLedGroup group3;
    private ListLedGroup group4;
    
    private readonly CorsairColor red = new CorsairColor(255, 0, 0);
    private readonly CorsairColor blue = new CorsairColor(0, 0, 255);
    private readonly CorsairColor green = new CorsairColor(0, 255, 0);
    private readonly CorsairColor yellow = new CorsairColor(255, 255, 0);

    private void Start() {
        keyboard = KeyboardManager.Instance.RgbKeyboard;
        if (keyboard == null) return;
        keyboard.Brush = (SolidColorBrush) CorsairColor.Transparent;
        group1 = new ListLedGroup(keyboard, LedsOne);
        group2 = new ListLedGroup(keyboard, LedsTwo);
        group3 = new ListLedGroup(keyboard, LedsThree);
        group4 = new ListLedGroup(keyboard, LedsFour);

        group1.Brush = (SolidColorBrush) red;
        group2.Brush = (SolidColorBrush) red;
        group3.Brush = (SolidColorBrush) blue;
        group4.Brush = (SolidColorBrush) blue;

    }

    public void OnEnterBlue() {
        background.sprite = sprites[0];
        if (keyboard == null) return;
        group1.Brush = (SolidColorBrush) red;
        group2.Brush = (SolidColorBrush) red;
        group3.Brush = (SolidColorBrush) blue;
        group4.Brush = (SolidColorBrush) blue;
    }

    public void OnEnterGreen() {
        background.sprite = sprites[1];
        if (keyboard == null) return;
        group1.Brush = (SolidColorBrush) red;
        group2.Brush = (SolidColorBrush) green;
        group3.Brush = (SolidColorBrush) green;
        group4.Brush = (SolidColorBrush) blue;

    }

    public void OnEnterYellow() {
        background.sprite = sprites[2];
        if (keyboard == null) return;
        group1.Brush = (SolidColorBrush) red;
        group2.Brush = (SolidColorBrush) green;
        group3.Brush = (SolidColorBrush) yellow;
        group4.Brush = (SolidColorBrush) blue;
    }

    public void OnEnterRed() {
        background.sprite = sprites[3];
        if (keyboard == null) return;
        group1.Brush = (SolidColorBrush) red;
        group2.Brush = (SolidColorBrush) red;
        group3.Brush = (SolidColorBrush) red;
        group4.Brush = (SolidColorBrush) red;
    }
    
    public void QuitTheGame() {
        Application.Quit();
    }

    public void StartTheGame(int playerCount) {
        ThePlayerCount = playerCount;
        if (keyboard != null) {
            keyboard.DetachLedGroup(group1);
            keyboard.DetachLedGroup(group2);
            keyboard.DetachLedGroup(group3);
            keyboard.DetachLedGroup(group4);
        }

        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    private readonly IEnumerable<CorsairLedId>  LedsOne = new[] {
        CorsairLedId.Escape,
        CorsairLedId.F1,
        CorsairLedId.F2,
        CorsairLedId.F3,
        CorsairLedId.F4,
        CorsairLedId.GraveAccentAndTilde,
        CorsairLedId.D1,
        CorsairLedId.D2,
        CorsairLedId.D3,
        CorsairLedId.D4,
        CorsairLedId.D5,
        CorsairLedId.Tab,
        CorsairLedId.Q,
        CorsairLedId.W,
        CorsairLedId.E,
        CorsairLedId.R,
        CorsairLedId.CapsLock,
        CorsairLedId.A,
        CorsairLedId.S,
        CorsairLedId.D,
        CorsairLedId.F,
        CorsairLedId.LeftShift,
        CorsairLedId.Z,
        CorsairLedId.X,
        CorsairLedId.C,
        CorsairLedId.LeftCtrl,
        CorsairLedId.LeftGui,
        CorsairLedId.LeftAlt
    }; 

    private readonly IEnumerable<CorsairLedId>  LedsTwo = new[] {
        CorsairLedId.F5,
        CorsairLedId.F6,
        CorsairLedId.F7,
        CorsairLedId.F8,
        CorsairLedId.D6,
        CorsairLedId.D7,
        CorsairLedId.D8,
        CorsairLedId.D9,
        CorsairLedId.D0,
        CorsairLedId.T,
        CorsairLedId.Y,
        CorsairLedId.U,
        CorsairLedId.I,
        CorsairLedId.O,
        CorsairLedId.G,
        CorsairLedId.H,
        CorsairLedId.J,
        CorsairLedId.K,
        CorsairLedId.L,
        CorsairLedId.V,
        CorsairLedId.B,
        CorsairLedId.N,
        CorsairLedId.M,
        CorsairLedId.PeriodAndBiggerThan,
        CorsairLedId.CommaAndLessThan,
        CorsairLedId.Space,
        CorsairLedId.RightAlt
    }; 
        
    private readonly IEnumerable<CorsairLedId>  LedsThree = new[] {
        CorsairLedId.F9,
        CorsairLedId.F10,
        CorsairLedId.F11,
        CorsairLedId.F12,
        CorsairLedId.PrintScreen,
        CorsairLedId.MinusAndUnderscore,
        CorsairLedId.EqualsAndPlus,
        CorsairLedId.Backspace,
        CorsairLedId.Insert,
        CorsairLedId.P,
        CorsairLedId.BracketLeft,
        CorsairLedId.BracketRight,
        CorsairLedId.Backslash,
        CorsairLedId.Delete,
        CorsairLedId.SemicolonAndColon,
        CorsairLedId.ApostropheAndDoubleQuote,
        CorsairLedId.Enter,
        CorsairLedId.SlashAndQuestionMark,
        CorsairLedId.RightShift,
        CorsairLedId.Fn,
        CorsairLedId.Application,
        CorsairLedId.RightCtrl,
        CorsairLedId.LeftArrow,
    }; 

    private readonly IEnumerable<CorsairLedId>  LedsFour = new[] {
        CorsairLedId.ScrollLock,
        CorsairLedId.PauseBreak,
        CorsairLedId.Home,
        CorsairLedId.PageUp,
        CorsairLedId.NumLock,
        CorsairLedId.KeypadSlash,
        CorsairLedId.KeypadAsterisk,
        CorsairLedId.KeypadMinus,
        CorsairLedId.End,
        CorsairLedId.PageDown,
        CorsairLedId.Keypad7,
        CorsairLedId.Keypad8,
        CorsairLedId.Keypad9,
        CorsairLedId.KeypadPlus,
        CorsairLedId.Keypad4,
        CorsairLedId.Keypad5,
        CorsairLedId.Keypad6,
        CorsairLedId.UpArrow,
        CorsairLedId.Keypad1,
        CorsairLedId.Keypad2,
        CorsairLedId.Keypad3,
        CorsairLedId.KeypadEnter,
        CorsairLedId.DownArrow,
        CorsairLedId.RightArrow,
        CorsairLedId.Keypad0,
        CorsairLedId.KeypadPeriodAndDelete
    }; 
    
    void OnApplicationQuit() {
        if (keyboard == null) return;
        keyboard.DetachLedGroup(group1);
        keyboard.DetachLedGroup(group2);
        keyboard.DetachLedGroup(group3);
        keyboard.DetachLedGroup(group4);

        if (CueSDK.IsInitialized)
            CueSDK.Reinitialize();
    }

}
