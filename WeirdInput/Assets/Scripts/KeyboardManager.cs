﻿using System;
using System.Collections.Generic;
using CUE.NET;
using CUE.NET.Brushes;
using CUE.NET.Devices.Generic;
using CUE.NET.Devices.Generic.Enums;
using CUE.NET.Devices.Keyboard;
using UnityEngine;

/// <summary>
/// Singleton component, attach this to one gameobject pls
/// </summary>
public class KeyboardManager : MonoBehaviour
{
    public static KeyboardManager Instance;
    public CorsairKeyboard RgbKeyboard;
    
    private void Awake ()
	{
	    if (Instance != null)
	    {
            Destroy(this);
	        return;
	    }

	    Instance = this;
        Intialise();
	}

    private void Intialise()
    {
        try {
            CueSDK.PossibleX64NativePaths.Add(Application.dataPath + "/plugins/CUE.NET/CUESDK_2015_x64.dll");
            CueSDK.PossibleX86NativePaths.Add(Application.dataPath + "/plugins/CUE.NET/CUESDK_2015_x86.dll");

            if (!CueSDK.IsInitialized)
                CueSDK.Initialize();

            RgbKeyboard = CueSDK.KeyboardSDK;
            if (RgbKeyboard == null) return;
            RgbKeyboard.Brush = (SolidColorBrush) CorsairColor.Transparent;

            CueSDK.UpdateMode = UpdateMode.Continuous;
    }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }

    public static Dictionary<KeyCode, CorsairLedId> UnityKeyCodeToCorsair = new Dictionary<KeyCode, CorsairLedId>()
    {
        {KeyCode.A, CorsairLedId.A},
        {KeyCode.B, CorsairLedId.B},
        {KeyCode.C, CorsairLedId.C},
        {KeyCode.D, CorsairLedId.D},
        {KeyCode.E, CorsairLedId.E},
        {KeyCode.F, CorsairLedId.F},
        {KeyCode.G, CorsairLedId.G},
        {KeyCode.H, CorsairLedId.H},
        {KeyCode.I, CorsairLedId.I},
        {KeyCode.J, CorsairLedId.J},
        {KeyCode.K, CorsairLedId.K},
        {KeyCode.L, CorsairLedId.L},
        {KeyCode.M, CorsairLedId.M},
        {KeyCode.N, CorsairLedId.N},
        {KeyCode.O, CorsairLedId.O},
        {KeyCode.P, CorsairLedId.P},
        {KeyCode.Q, CorsairLedId.Q},
        {KeyCode.R, CorsairLedId.R},
        {KeyCode.S, CorsairLedId.S},
        {KeyCode.T, CorsairLedId.T},
        {KeyCode.U, CorsairLedId.U},
        {KeyCode.V, CorsairLedId.V},
        {KeyCode.W, CorsairLedId.W},
        {KeyCode.X, CorsairLedId.X},
        {KeyCode.Y, CorsairLedId.Y},
        {KeyCode.Z, CorsairLedId.Z},
        {KeyCode.Alpha0, CorsairLedId.D0},
        {KeyCode.Alpha1, CorsairLedId.D1},
        {KeyCode.Alpha2, CorsairLedId.D2},
        {KeyCode.Alpha3, CorsairLedId.D3},
        {KeyCode.Alpha4, CorsairLedId.D4},
        {KeyCode.Alpha5, CorsairLedId.D5},
        {KeyCode.Alpha6, CorsairLedId.D6},
        {KeyCode.Alpha7, CorsairLedId.D7},
        {KeyCode.Alpha8, CorsairLedId.D8},
        {KeyCode.Alpha9, CorsairLedId.D9}
    };

    void OnApplicationQuit() {
        if (RgbKeyboard == null) return;
        foreach (var led in RgbKeyboard.Leds) {
            led.Color = new CorsairColor(0, 0, 0);
        }

        if (CueSDK.IsInitialized)
            CueSDK.Reinitialize();
    }
}
