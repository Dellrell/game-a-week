﻿using System.Collections;
using System.Collections.Generic;
using CUE.NET;
using CUE.NET.Brushes;
using CUE.NET.Devices.Generic;
using CUE.NET.Devices.Keyboard;
using UnityEngine;

public class Test : MonoBehaviour {

	// Use this for initialization
	void Start () {
		CueSDK.Initialize();
	    CorsairKeyboard keyboard = CueSDK.KeyboardSDK;
	    keyboard.Brush = (SolidColorBrush) CorsairColor.Transparent;
        keyboard['A'].Color = new CorsairColor(0, 0, 255);
        keyboard.Update();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnApplicationQuit() {
        CueSDK.Reinitialize();
    }
}
