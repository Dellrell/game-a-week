﻿using CUE.NET.Devices.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManagers : MonoBehaviour {

    private readonly CorsairColor off = new CorsairColor(0, 0, 0);

    public void RestartGame() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        if (KeyboardManager.Instance.RgbKeyboard == null) return;
        foreach (var led in KeyboardManager.Instance.RgbKeyboard.Leds) {
            led.Color = off;
        }
    }

    public void QuitGame() {
        Application.Quit();
    }
}
