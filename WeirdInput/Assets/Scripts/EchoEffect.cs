﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using CUE.NET;
using CUE.NET.Brushes;
using CUE.NET.Devices.Generic;
using CUE.NET.Devices.Generic.Enums;
using CUE.NET.Devices.Keyboard;

public class EchoEffect : MonoBehaviour
{
    public float FadeDuration = 1f;
    public float LitDuration = 1f;

    public Color StartingColor;
    public bool RandomColor = false;

    /// <summary>
    /// A structure to store all active echo coroutines
    /// </summary>
    private readonly Dictionary<CorsairLedId, IEnumerator> _activeKeyCoroutines = new Dictionary<CorsairLedId, IEnumerator>();

	private void Update () {

	    foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode))) 
	    {
	        if (!Input.GetKeyDown(vKey) || !KeyboardManager.UnityKeyCodeToCorsair.ContainsKey(vKey)) {continue;}

	        var key = KeyboardManager.UnityKeyCodeToCorsair[vKey];


            // if we're already doing an echo effect for this key, then stop it and start a new one
            if (_activeKeyCoroutines.ContainsKey(key)) {

                StopCoroutine(_activeKeyCoroutines[key]);
                _activeKeyCoroutines.Remove(key);
            }

            var enumerator = CreateEchoEffect(key);
	        _activeKeyCoroutines.Add(key, enumerator);

	        StartCoroutine(enumerator);
	    }
    }

    /// <summary>
    /// Starts a courtine that turns on a speficied LED and dims it over time
    /// </summary>
    /// <param name="id">The id you want to access</param>
    private IEnumerator CreateEchoEffect(CorsairLedId id)
    {  
        CorsairLed led = null;
        // Find the led on the keyboard that corresponds to the id provided
        //var led = KeyboardManager.Instance.RgbKeyboard.GetLeds().First(l => l.Id == id);
        foreach (var rgbLed in KeyboardManager.Instance.RgbKeyboard.GetLeds()) {
            if (rgbLed.Id == id) {
                led = rgbLed;
            }
                
        }

        if (led == null) {
            yield break;
        }
        
        var timeSinceStart = 0f;
        var originalColor = GetStartingColour();

        while (timeSinceStart < FadeDuration + LitDuration)
        {
            // set the alpha to fade out, but to not go over 1
            originalColor.a = Mathf.Min((FadeDuration + LitDuration - timeSinceStart) / FadeDuration, 1f);
            // convert the colour and apply the alpha channel
            led.Color = GetCorsairColour(originalColor);
            // wait till the next frame
            yield return null;
            timeSinceStart += Time.deltaTime;
        }
    }

    /// <summary>
    /// Returns either a new colour or a selected color
    /// </summary>
    private Color GetStartingColour()
    {
        return !RandomColor ? StartingColor : Color.HSVToRGB(Random.Range(0, 1), 1, 1);
    }

    /// <summary>
    /// Returns a corsair colour from a unity colour
    /// </summary>
    /// <param name="originalColour">The unity colour</param>
    /// <returns></returns>
    private CorsairColor GetCorsairColour(Color originalColour)
    {
        // apply alpha to color
        return new CorsairColor((byte)(originalColour.r * originalColour.a * 255), (byte)(originalColour.g * originalColour.a * 255), (byte)(originalColour.b * originalColour.a * 255));
    }
}
