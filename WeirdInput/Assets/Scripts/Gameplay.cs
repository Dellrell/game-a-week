﻿using System.Collections.Generic;
using System.Linq;
using CUE.NET;
using CUE.NET.Brushes;
using CUE.NET.Devices.Generic;
using CUE.NET.Devices.Generic.Enums;
using CUE.NET.Devices.Keyboard;
using UnityEngine;
using Random = UnityEngine.Random;

public class Gameplay : MonoBehaviour {
    private CorsairKeyboard keyboard;

    private List<KeyCode> possibleKeys = new List<KeyCode>() {
        KeyCode.A,
        KeyCode.B,
        KeyCode.C,
        KeyCode.D,
        KeyCode.E,
        KeyCode.F,
        KeyCode.G,
        KeyCode.H,
        KeyCode.I,
        KeyCode.J,
        KeyCode.K,
        KeyCode.L,
        KeyCode.M,
        KeyCode.N,
        KeyCode.O,
        KeyCode.P,
        KeyCode.Q,
        KeyCode.R,
        KeyCode.S,
        KeyCode.T,
        KeyCode.U,
        KeyCode.V,
        KeyCode.W,
        KeyCode.X,
        KeyCode.Y,
        KeyCode.Z,
        KeyCode.Alpha0,
        KeyCode.Alpha1,
        KeyCode.Alpha2,
        KeyCode.Alpha3,
        KeyCode.Alpha4,
        KeyCode.Alpha5,
        KeyCode.Alpha6,
        KeyCode.Alpha7,
        KeyCode.Alpha8,
        KeyCode.Alpha9
    };

    public KeyCode[] actualKeys;
    private CorsairLed[] actualLeds;

    private readonly CorsairColor off = new CorsairColor(0, 0, 0);
    private readonly CorsairColor red = new CorsairColor(255, 0, 0);
    private readonly CorsairColor blue = new CorsairColor(0, 0, 255);
    private readonly CorsairColor green = new CorsairColor(0, 255, 0);
    private readonly CorsairColor yellow = new CorsairColor(255, 255, 0);
    
    public float player1Timer = 30;
    public float player2Timer = 30;
    public float player3Timer = 30;
    public float player4Timer = 30;

    private bool[] playerStatuses;

    private bool gameInProgress = false;
    private bool gameEnded = false;

    private float switchTime = 8f;
    private float ledTimer;

    private UIControl uicontrol;

    private void OnEnable() {
       switch (PlayerCount.ThePlayerCount) {
            case 2:
                actualKeys = new KeyCode[4];
                actualLeds = new CorsairLed[4];
                playerStatuses = new bool[2];
                break;
            case 3:
                actualKeys = new KeyCode[6];
                actualLeds = new CorsairLed[6];
                playerStatuses = new bool[3];
                break;
            case 4:
                actualKeys = new KeyCode[8];
                actualLeds = new CorsairLed[8];
                playerStatuses = new bool[4];
                break;
            default:
                actualKeys = new KeyCode[4];
                actualLeds = new CorsairLed[4];
                playerStatuses = new bool[2];
                break;
        }

        for (int i = 0; i < actualKeys.Length; i++) {
            var tkey = possibleKeys[Random.Range(0, possibleKeys.Count)];
            possibleKeys.Remove(tkey);
            actualKeys[i] = tkey;
            if (KeyboardManager.Instance.RgbKeyboard == null) continue;
            actualLeds[i] = GetMatchingLeds(KeyboardManager.UnityKeyCodeToCorsair[tkey]);
        }

        for (int i = 0; i < playerStatuses.Length; i++) {
            playerStatuses[i] = true;
        }

        uicontrol = FindObjectOfType<UIControl>();

        keyboard = KeyboardManager.Instance.RgbKeyboard;

        if (keyboard == null) return;
        keyboard.Brush = (SolidColorBrush)CorsairColor.Transparent;

        foreach (var led in keyboard.Leds) {
            led.Color = off;
        }


        actualLeds[0].Color = red;
        actualLeds[1].Color = red;

        actualLeds[2].Color = blue;
        actualLeds[3].Color = blue;

        if (actualKeys.Length > 4) {
            actualLeds[4].Color = green;
            actualLeds[5].Color = green;

            if (actualKeys.Length > 6) {
                actualLeds[6].Color = yellow;
                actualLeds[7].Color = yellow;
                
            }
        }
    }

    private void Update() {
        
        if (!gameInProgress && !gameEnded && Input.anyKeyDown) { gameInProgress = true; }

        if (gameInProgress) {

            if (!Input.GetKey(actualKeys[0]) || !Input.GetKey(actualKeys[1])) {
                player1Timer -= Time.deltaTime;
                if (player1Timer <= 0) {
                    playerStatuses[0] = false;
                }
            }

            if (!Input.GetKey(actualKeys[2]) || !Input.GetKey(actualKeys[3])) {
                player2Timer -= Time.deltaTime;
                if (player2Timer <= 0) {
                    playerStatuses[1] = false;
                }
            }

            if (actualKeys.Length > 4) {
                if (!Input.GetKey(actualKeys[4]) || !Input.GetKey(actualKeys[5])) {
                    player3Timer -= Time.deltaTime;
                    if (player3Timer <= 0) {
                        playerStatuses[2] = false;
                    }
                }

                if (actualKeys.Length > 6) {
                    if (!Input.GetKey(actualKeys[6]) || !Input.GetKey(actualKeys[7])) {
                        player4Timer -= Time.deltaTime;
                        if (player4Timer <= 0) {
                            playerStatuses[3] = false;
                        }
                    }
                }
            }

            CheckForWinner();

            ledTimer += Time.deltaTime;
            if(ledTimer >= switchTime)
                SwapKeys();
        }
    }

    private void CheckForWinner() {
        int trueCount = playerStatuses.Count(b => b);

        if (trueCount == 1) {
            DeclareWinner();
        }
    }

    private void DeclareWinner() {
        gameEnded = true;
        gameInProgress = false;
        int playerIndex = 0;

        for (int i = 0; i < playerStatuses.Length; i++) {
            if (playerStatuses[i])
                playerIndex = i;
        }

        uicontrol.ActiateEndScreen(playerIndex);

        if (keyboard == null) return;

        foreach (var led in actualLeds) {
            led.Color = off;
        }

        switch (playerIndex) {
            case 0:
                KeyboardManager.Instance.RgbKeyboard.Brush = (SolidColorBrush)red;
                print("Red PlayerWins!");
                break;
            case 1:
                KeyboardManager.Instance.RgbKeyboard.Brush = (SolidColorBrush)blue;
                print("Blue PlayerWins!");
                break;
            case 2:
                KeyboardManager.Instance.RgbKeyboard.Brush = (SolidColorBrush)green;
                print("Green PlayerWins!");
                break;
            case 3:
                KeyboardManager.Instance.RgbKeyboard.Brush = (SolidColorBrush)yellow;
                print("Yellow PlayerWins!");
                break;
            default:
                print("Something went wrong");
                break;
        }

        
    }


    private void SwapKeys() {
        ledTimer = 0;
        if(switchTime > 1)
            switchTime--;

        int index = Random.Range(0, actualKeys.Length);
        var oldKey = actualKeys[index];
        var newKey = possibleKeys[Random.Range(0, possibleKeys.Count)];
        actualKeys[index] = newKey;
       
        
        possibleKeys.Remove(newKey);
        possibleKeys.Add(oldKey);

        if (keyboard == null) return;
        var oldLed = actualLeds[index];
        var newLed = GetMatchingLeds(KeyboardManager.UnityKeyCodeToCorsair[newKey]);
        actualLeds[index] = newLed;
        oldLed.Color = off;

        switch (index) {
           case 0:
           case 1:
               actualLeds[index].Color = red;
                break;
            case 2:
            case 3:
                actualLeds[index].Color = blue;
                break;
            case 4:
            case 5:
                actualLeds[index].Color = green;
                break;
            case 6:
            case 7:
                actualLeds[index].Color = yellow;
                break;
            default:
                print("index fucked up");
                break;
        }
    }

    private CorsairLed GetMatchingLeds(CorsairLedId id) {

        CorsairLed led = null;
        
        foreach (var rgbLed in KeyboardManager.Instance.RgbKeyboard.GetLeds()) {
            if (rgbLed.Id == id) {
                led = rgbLed;
            }

        }

        return led;
    }
}
