﻿using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour {

    public GameObject[] playerIndicators;
    public Text[] keyText;
    public GameObject[] endScreens;

    private Gameplay gameplay;

    private float alpha1;
    private float alpha2;
    private float alpha3;
    private float alpha4;

    private Image image1;
    private Image image2;
    private Image image3;
    private Image image4;



	void Awake () {
	    gameplay = FindObjectOfType<Gameplay>();
	    switch (PlayerCount.ThePlayerCount) {
	        case 2:
	            playerIndicators[0].SetActive(true);
	            playerIndicators[1].SetActive(true);
	            playerIndicators[2].SetActive(false);
	            playerIndicators[3].SetActive(false);
	            break;
	        case 3:
	            playerIndicators[0].SetActive(true);
	            playerIndicators[1].SetActive(true);
	            playerIndicators[2].SetActive(true);
	            playerIndicators[3].SetActive(false);
	            break;
	        case 4:
	            playerIndicators[0].SetActive(true);
	            playerIndicators[1].SetActive(true);
	            playerIndicators[2].SetActive(true);
	            playerIndicators[3].SetActive(true);
	            break;
	        default:
	            playerIndicators[0].SetActive(true);
	            playerIndicators[1].SetActive(true);
	            playerIndicators[2].SetActive(false);
	            playerIndicators[3].SetActive(false);
	            break;
	    }
	    image1 = playerIndicators[0].GetComponent<Image>();
	    image2 = playerIndicators[1].GetComponent<Image>();
	    image3 = playerIndicators[2].GetComponent<Image>();
	    image4 = playerIndicators[3].GetComponent<Image>();

	}
	
	void Update () {
	    for (int i = 0; i < gameplay.actualKeys.Length; i++) {
	        keyText[i].text = gameplay.actualKeys[i].ToString();
	    }

	    alpha1 = gameplay.player1Timer / 30;
	    alpha2 = gameplay.player2Timer / 30;
	    alpha3 = gameplay.player3Timer / 30;
	    alpha4 = gameplay.player4Timer / 30;

        image1.color = new Color(1, 0, 0, alpha1);
	    image2.color = new Color(0, 0, 1, alpha2);
	    image3.color = new Color(0, 1, 0, alpha3);
	    image4.color = new Color(1, 1, 0, alpha4);

	    
	}

    public void ActiateEndScreen(int index) {
        endScreens[index].SetActive(true);
    }
}
