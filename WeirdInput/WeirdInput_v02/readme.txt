
Game A Week ~ Project 9 ~ Weird Input
Rebecca Dilella
=================================================================================================================================================================
Game: Keyboard Twister

This game is intended to be played with an RGB keyboard, specifically a Corsair rgb keyboard, with Corsair's CUE software running in the background.
However it is playable without these, you just have to rely on the UI to indicate which buttons to press instead of any lights ona keyboard.

It is a competitive game for 2-4 players. Players must hold down the buttons of their own colour, while attemting to prevent other
players from holding down their own buttons. The buttons will change randomly, the changes will happen faster the longer the game continues. 
If players let go if either of their buttons for long enough they are eliminated, until only one winner remains. 
=================================================================================================================================================================

For this game I made use of Corsair's CUE software, as well as the CUE SDK
I also made use of CUE.NET, a C#-library build around the Corsair CUE-SDK, by DarthAffe 
<https://github.com/DarthAffe/CUE.NET/wiki>
I was therefore able to write all the game's code in C# and run it from unity using plugins