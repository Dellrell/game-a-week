﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnIceCream : MonoBehaviour {

    public GameObject[] IceCreams;
    private bool empty;
    private float spawnTimer;
    private float spawnTime = 3;
    
    
    void Start() {
        int index = Random.Range(0, IceCreams.Length);  
        Instantiate(IceCreams[index], transform.position, Quaternion.Euler(25, 0, 0), transform);
    }

    void Update() {
        if (transform.childCount < 1) {
            spawnTimer += Time.deltaTime;
            if (spawnTimer >= spawnTime) {
                int index = Random.Range(0, IceCreams.Length - 1);  
                Instantiate(IceCreams[index], transform.position, Quaternion.Euler(25, 0, 0), transform);
                spawnTimer = 0;
            }
        }
    }


}
