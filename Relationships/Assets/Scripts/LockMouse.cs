﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockMouse : MonoBehaviour {

    private MenuManager menuManager;

    void Start() {
        menuManager = GetComponent<MenuManager>();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

	void Update () {
	    if (menuManager.GameStarted) {
	        Cursor.lockState = CursorLockMode.Locked;
	        Cursor.visible = false;

	        if (Input.GetKey(KeyCode.Escape))
	            Cursor.lockState = CursorLockMode.None;
	    }

	    if (menuManager.GameEnded) {
	        Cursor.lockState = CursorLockMode.None;
	        Cursor.visible = true;
	    }
	}
}
