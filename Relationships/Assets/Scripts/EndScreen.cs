﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Color = UnityEngine.Color;

public class EndScreen : MonoBehaviour {

    private Image background;
    private TextMeshProUGUI text;

    private float endTimer;

	// Use this for initialization
	void Awake () {
	    background = GetComponent<Image>();
	    text = GetComponentInChildren<TextMeshProUGUI>();

        background.color = Color.clear;
        text.color = Color.clear;


	}
	
	// Update is called once per frame
	void Update () {
	    background.color = Color.Lerp(background.color, Color.black, 0.01f);
	    text.color = Color.Lerp(text.color, Color.white, 0.005f);

	    endTimer += Time.deltaTime;
	    if (endTimer > 10) {
	        GameObject buttonsObj = transform.GetChild(1).gameObject;
            buttonsObj.SetActive(true);
	    }
	}
}
