﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobAndSpin : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        transform.Rotate(new Vector3(0, 45 * Time.deltaTime, 0), Space.World);

        transform.position += new Vector3(0, 0.5f*Mathf.Sin(2*Time.time), 0) * Time.deltaTime;

	}
}
