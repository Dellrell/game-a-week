﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public GameObject startMenu;
    public GameObject endScreen;
    public PlayerMovement playerMovement;
    public MouseLook mouseLook;
    public HealthBar healthBar;
    public bool GameStarted = false;
    public bool GameEnded = false;
    public float StartTime;

    public void StartGame() {
        startMenu.SetActive(false);
        GameStarted = true;
        StartTime = Time.time;
        playerMovement.enabled = true;
        mouseLook.enabled = true;
        healthBar.enabled = true;
    }

    public void EndGame() {
        endScreen.SetActive(true);
        GameEnded = true;
        playerMovement.enabled = false;
        mouseLook.enabled = false;
        healthBar.enabled = false;
    }

    public void ResetGame() {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void QuitGame() {
        Application.Quit();
        Debug.Log("Quit");
    }

}
