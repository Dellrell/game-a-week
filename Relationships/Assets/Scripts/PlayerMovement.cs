﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed = 5.0f;
    public float gravity = 10.0f;
    public float jumpSpeed = 6.0f;

    private float horizontalInput;
    private float verticalInput;
    private int jumpTimer;
    private int jumpReset = 1;
    private CharacterController controller;
    private Vector3 movement = Vector3.zero;

    private int npcLayer = 1 << 9;
    private RaycastHit hit;
    private GameObject hitObj;

	// Use this for initialization
	void Start () {
	    controller = GetComponent<CharacterController>();
	}
	
    void Update() {

        if (Physics.Raycast(transform.position, transform.forward, out hit, 5.0f, npcLayer)) {
            hitObj = hit.transform.gameObject;
            Transform child = hitObj.transform.GetChild(0);
            child.gameObject.SetActive(true);
        } else if (hitObj != null) {
            Transform child = hitObj.transform.GetChild(0);
            child.gameObject.SetActive(false);
            hitObj = null;
        }
    }

	void FixedUpdate () {
        //get input
	    horizontalInput = Input.GetAxis("Horizontal");
	    verticalInput = Input.GetAxis("Vertical");

	    if (controller.isGrounded) {
            //apply input
	        movement = new Vector3(horizontalInput, 0, verticalInput);
            movement = transform.TransformDirection(movement) * speed;

            if (!Input.GetButton("Jump")) {
	            jumpTimer++;
	        } else if (jumpTimer >= jumpReset) {
	            movement.y = jumpSpeed;
	            jumpTimer = 0;
	        }

	    }

	    //apply gravity
	    movement.y -= gravity * Time.deltaTime;
	    //move controller
	    controller.Move(movement * Time.deltaTime);

    }
}
