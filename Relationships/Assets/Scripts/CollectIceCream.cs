﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectIceCream : MonoBehaviour {

    private HealthBar healthBar;

    void Start() {
        healthBar = GetComponent<HealthBar>();
    }

    void OnTriggerEnter(Collider other) {
        //Debug.Log("triggered");
        if (other.tag == "IceCream") {
            Destroy(other.gameObject);
            healthBar.IncreaseHealth();
        }
    }

}
