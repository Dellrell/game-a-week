﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class PostProcessingController : MonoBehaviour {

    public PostProcessingProfile profile;
    public GameObject text1;
    public GameObject text2;
    public bool ok;

    private ColorGradingModel colourGrading;
    private DepthOfFieldModel depthOfField;
    private ColorGradingModel.Settings colourSettings;
    private DepthOfFieldModel.Settings depthSettings;
    private float saturation = 2;
    private float focalLength = 300;

    private float startTime;
    private bool coroutineRunning = false;

    public MenuManager menuManager;

	// Use this for initialization
	void Start () {
	    colourGrading = profile.colorGrading;
	    depthOfField = profile.depthOfField;
	}

   
	// Update is called once per frame
	void Update () {

	    depthSettings = depthOfField.settings;
	    depthSettings.focalLength = focalLength;

	    if (menuManager.GameStarted)
	        focalLength = 0;

	    colourSettings = colourGrading.settings;
	    colourSettings.basic.saturation = saturation;

	    if (Mathf.Abs(menuManager.StartTime - Time.time) > 30 && !ok && menuManager.GameStarted) { 
	        saturation -= Time.deltaTime * 0.1f;
	        saturation = Mathf.Clamp(saturation, 0, 2);
	    }

	    if (saturation <= 0 && !ok) {
	        int chance1 = Random.Range(0, 1000);
	        int chance2 = Random.Range(0, 1000);

	        text1.transform.position += new Vector3(Random.insideUnitCircle.x, Random.insideUnitCircle.y, 0);
	        text2.transform.position += new Vector3(Random.insideUnitCircle.x, Random.insideUnitCircle.y, 0);

	        if (chance1 == 1 && !text1.activeInHierarchy && !coroutineRunning) {
	            StartCoroutine(FlashText(text1));
	        }

	        if (chance2 == 1 && !text2.activeInHierarchy && !coroutineRunning) {
	            StartCoroutine(FlashText(text2));
	        }
	    }

	    if (ok) {
	        saturation += Time.deltaTime;
	        saturation = Mathf.Clamp(saturation, 0, 2);
	    }

	    depthOfField.settings = depthSettings;
	    colourGrading.settings = colourSettings;
	}

    private IEnumerator FlashText(GameObject text) {
        coroutineRunning = true;
        text.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        text.SetActive(false);
        coroutineRunning = false;
        yield return null;
        
    }
}
