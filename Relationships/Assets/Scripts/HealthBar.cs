﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public Slider healthBar;

    public GameObject[] spawners;
    public PostProcessingController ppcontrol;

    private const float fullHealth = 10.0f;
    private float currentHealth;
    private Transform barPos;
    private Vector3 barDefaultPos;
    private float shakeMagnitude;
    private float deltaHealth;

    private float okTimer;

    public MenuManager menuManager;

	// Use this for initialization
	void Start () {
	    currentHealth = fullHealth;
	    healthBar.maxValue = fullHealth;
	    barPos = healthBar.transform;
	    barDefaultPos = barPos.position;
	}
	
	// Update is called once per frame
	void Update () {
	    healthBar.value = currentHealth;
	    currentHealth = Mathf.Clamp(currentHealth, 0, fullHealth);
	    
        if (okTimer < 5)
	        DrainHealth();

	    if (currentHealth <= 0) {
	        okTimer += Time.deltaTime;
	    }
	    else {
	        okTimer = 0;
	    }

	    if (okTimer >= 5) {
	        foreach (GameObject spawner in spawners) {
	            spawner.SetActive(false);
	        }

	        ppcontrol.ok = true;
	        Image background = healthBar.transform.GetChild(0).GetComponent<Image>();
            Color colour = Color.green;
	        background.color = Color.Lerp(background.color, colour, 0.01f);

	    }

	    if (okTimer >= 12) {
            menuManager.EndGame();
	    }
        
	}

    void DrainHealth() {

        currentHealth -= Time.deltaTime * 0.5f;
        deltaHealth = fullHealth - currentHealth;
        shakeMagnitude = Mathf.Abs(deltaHealth) *0.3f;

        barPos.position = barDefaultPos + (new Vector3(Random.insideUnitCircle.x, Random.insideUnitCircle.y, 0) * shakeMagnitude);

    }

    public void IncreaseHealth() {
        currentHealth += 2;
    }
}
